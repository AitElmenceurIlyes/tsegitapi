package com.api;
public class Constant {
    public static final String BASE_URL ="https://code.telecomste.fr";
    public static final String API_PATH= "/api/v4/";
    public static final String OAUTH_PATH= "/oauth/token";
    public static final String USER_PATH ="users/";
    
}
