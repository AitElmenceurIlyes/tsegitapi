package com.api.service;

import java.util.List;


import com.api.Constant;
import com.model.Project;
import com.model.UserGit;

import org.gitlab4j.api.models.Key;
import org.gitlab4j.api.models.Membership;
import org.gitlab4j.api.models.OauthTokenResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserService {

        @POST(Constant.OAUTH_PATH+"?grant_type=password")
        public Call<OauthTokenResponse> Connect(@Query("username") String username,
        @Query("password") String string);

        @GET(Constant.API_PATH+Constant.USER_PATH)
        public Call<List<UserGit>> getUserByUsername(@Query("username") String username);

        @GET(Constant.API_PATH+Constant.USER_PATH + "{user_id}")
        public Call<UserGit> getUserByID(@Header("Authorization") String token,
                        @Path("user_id") int user_id);

        @GET(Constant.API_PATH+Constant.USER_PATH + "{user_id}/projects")
        public Call<List<Project>> getUserProjet(@Header("Authorization") String token,
                        @Path("user_id") int user_id,
                        @Query("statistics") boolean stat );

        @GET(Constant.API_PATH+Constant.USER_PATH + "{user_id}/memberships")
        public Call<List<Membership>> getUserMembership(@Header("Authorization") String token,
                        @Path("user_id") int id); // ne marche que pour admin

        @GET(Constant.API_PATH+Constant.USER_PATH + "{user_id}/keys")
        public Call<List<Key>> getKey(@Header("Authorization") String token,
                        @Path("user_id") int id); // ne marche que pour admin

}