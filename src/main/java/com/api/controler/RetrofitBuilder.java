package com.api.controler;

import java.net.URL;

import com.api.Constant;

import org.gitlab4j.api.utils.JacksonJson;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RetrofitBuilder {
    private static RetrofitBuilder single_instance = null;
 
    private static Retrofit retrofit;

    private static URL BASE_URL;

 
    private RetrofitBuilder()
    {
        JacksonJson jacksonJson = new JacksonJson();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
         
        retrofit = new Retrofit.Builder()
          .baseUrl(BASE_URL)
          .addConverterFactory(JacksonConverterFactory.create(jacksonJson.getObjectMapper()))
          .client(httpClient.build())
          .build();
    }
 
    
    /** 
     * @return RetrofitBuilder
     */
    public static RetrofitBuilder getInstance()
    {
        if (single_instance == null)
            single_instance = new RetrofitBuilder();
 
        return single_instance;
    }
    
    /** 
     * @return Retrofit
     */
    public static Retrofit getRetrofit(){
        return retrofit;
    };
    
    /** 
     * Set base url of the API
     * @param url
     */
    public static void setURL(URL url){
        BASE_URL=url;
    };
}

         
        