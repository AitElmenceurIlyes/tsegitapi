package com.api.controler;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import com.api.service.UserService;
import com.model.Project;
import com.model.UserGit;

import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Key;
import org.gitlab4j.api.models.Membership;
import org.gitlab4j.api.models.OauthTokenResponse;

import retrofit2.Call;
import retrofit2.Response;

public class UserAPI {
    private static UserService service;

    public UserAPI(URL url) {
        RetrofitBuilder.setURL(url);
        RetrofitBuilder.getInstance();
        
        service = RetrofitBuilder.getRetrofit()
                .create(UserService.class);
    }

    /**
     * Get user By his username
     * 
     * @param username user's username
     * @return
     * @return List<UserGit>
     * @throws GitLabApiException
     */
    public OauthTokenResponse Connect(String username, String string) throws GitLabApiException {
        Call<OauthTokenResponse> callSync = service.Connect(username, string);
        try {
            Response<OauthTokenResponse> response = callSync.execute();
            if(response.code()==400)
                throw new GitLabApiException("Incorrect password or username");
            OauthTokenResponse apiResponse = response.body();
            return apiResponse;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    
    /** 
     * @param username
     * @return List<UserGit>
     * @throws GitLabApiException
     */
    public List<UserGit> getUserByUsername(String username) throws GitLabApiException {
        Call<List<UserGit>> callSync = service.getUserByUsername(username);

        try {
            Response<List<UserGit>> response = callSync.execute();
            List<UserGit> apiResponse = response.body();
            if (apiResponse == null)
                throw new GitLabApiException("Incorrect Username");
            return apiResponse;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Get a user by his ID
     * 
     * @param token private token
     * @param id    user id
     * @return UserGit
     * @throws GitLabApiException
     */
    public UserGit getUserByID(String token, int id) throws GitLabApiException {
        Call<UserGit> callSync = service.getUserByID("Bearer " + token, id);

        Response<UserGit> response;
        try {
            response = callSync.execute();

            if (response.code() == 401)
                throw new GitLabApiException("401 Unauthorized");
            if (response.code() == 401)
                throw new GitLabApiException("404 User Not Found");
            UserGit apiResponse = response.body();
            if (apiResponse == null)
                throw new GitLabApiException("Incorrect id");
            return apiResponse;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    /**
     * Get Projects list of a user.
     * 
     * @param token private token
     * @param id    user id
     * @return List<Project>
     * @throws GitLabApiException
     */
    public List<Project> getUserProjet(String token, int id, boolean stat) throws GitLabApiException {

        Call<List<Project>> callSync = service.getUserProjet("Bearer " + token, id, stat);

        try {
            Response<List<Project>> response = callSync.execute();
            if (response.code() == 401)
                throw new GitLabApiException("401 Unauthorized");
            if (response.code() == 401)
                throw new GitLabApiException("404 User Not Found");
            List<Project> apiResponse = response.body();

            if (apiResponse == null)
                throw new GitLabApiException("Incorrect id");
            return apiResponse;

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Get list of ssh keys of a user
     * 
     * @param token private token
     * @param id    user id
     * @return List<Key>
     * @throws GitLabApiException
     */
    public List<Key> getKey(String token, int id) throws GitLabApiException {

        Call<List<Key>> callSync = service.getKey("Bearer " + token, id);

        try {
            Response<List<Key>> response = callSync.execute();
            if (response.code() == 401)
                throw new GitLabApiException("401 Unauthorized");
            if (response.code() == 401)
                throw new GitLabApiException("404 User Not Found");
            List<Key> apiResponse = response.body();
            if (apiResponse == null)
                throw new GitLabApiException("Incorrect id");
            return apiResponse;

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;

    }

    /**
     * Get the list of user membership (Projects,groups...). Only for Admin
     * 
     * @param token private token
     * @param id    user id
     * @return List<Membership>
     * @throws GitLabApiException
     */
    public List<Membership> getUserMembership(String token, int id) throws GitLabApiException {
        Call<List<Membership>> callSync = service.getUserMembership("Bearer " + token, id);

        try {
            Response<List<Membership>> response = callSync.execute();
            if (response.code() == 401)
                throw new GitLabApiException("401 Unauthorized");
            if (response.code() == 401)
                throw new GitLabApiException("404 User Not Found");
            List<Membership> apiResponse = response.body();
            if (apiResponse == null)
                throw new GitLabApiException("Incorrect id");
            return apiResponse;

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;

    }

}