package com.Main;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFrame;

import org.gitlab4j.api.GitLabApi;

import com.model.*;
import com.view.*;

import org.gitlab4j.api.GitLabApi;

import com.controller.*;



public class Main implements PropertyChangeListener {

	ConnectionView WelcomingWindow;
	CreateAccount CreateWindow;
	DashboardModel dashModel;
	DashboardView dashView;

    public Main() {
		WelcomingWindow = new ConnectionView();
		WelcomingWindow.setVisible(true);
		WelcomingWindow.addPropertyChangeListener(this);
    }
    
    
    
    
    
	/** 
	 * @param []
	 */
	public static void main(String args []) {
        new Main();
    }




	
	/** 
	 * @param evt
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		// TODO Auto-generated method stub
		JFrame obj = (JFrame) evt.getSource();
		//System.out.println(obj.toString());
		
		if (evt.getPropertyName() == "Connected") {

//	    	MyProjects pj_model = new MyProjects( (GitLabApi) evt.getNewValue());
//	        Project pj_project = new Project();
//	        ProjectController pj_cont = new ProjectController(pj_model, pj_project);
//	        ProjectView pj_view = new ProjectView(pj_model, pj_project, pj_cont);
//	        pj_cont.setView(pj_view);
			dashModel = new DashboardModel();
			dashView = new DashboardView();
			dashView.addPropertyChangeListener(this);
			obj.setVisible(false);
			obj.dispose();
		
		}
		else if (evt.getPropertyName() == "Disconnect" && (boolean)evt.getNewValue()) {
//			
			System.out.println("disconnect");
			obj.setVisible(false);
			obj.dispose();
			
			WelcomingWindow = new ConnectionView();
			WelcomingWindow.setVisible(true);
			WelcomingWindow.addPropertyChangeListener(this);

		}
		
	}
}
