package com.view;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.Member;

import com.controller.GitGudExceptions;
import com.controller.ProjectController;
import com.model.MyProjects;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;

import javax.swing.ImageIcon;


public class ProjectView extends JFrame implements ActionListener{
	
	//Model et controlleur de la view
	protected MyProjects model;
    protected ProjectController controler;
    
    //Composants de la view
	private JPanel contentPane;
	private JPanel panelButton;
	private JPanel usersPanel;
	private JPanel statsPanel;
	private JComboBox<String[]> comboBox;
	private JButton returnButton;
	private JButton[] btns_list;
    
	//Variable
	private int id;
	private int indexProject;
	private String nameProject;
	private String brancheChoosen;
	private int numberBranches;
	private String[] dossier;
	private String beforeState = "";


	/**
	 * Create the frame.
	 * @throws GitLabApiException 
	 */
	public ProjectView(String nameProject, int id, int indexProject,ProjectController controller) {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(ProjectView.class.getResource("/icon/sigle_telecom.png")));
	    this.controler = controller;
	    this.nameProject = nameProject;
	    this.id = id;
	    this.indexProject = indexProject;

		
		setTitle("Project : " + nameProject);
		setBounds(100, 100, 1000, 700);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		//On vérifie que il ya des branches (le projet n'est pas vide)
		try {
			//Si le projet existe, on crée une liste déroulante avec les branches du projet
			List<String> branches;
			branches = controller.getBranches(id);
			numberBranches = branches.size();
			String[] listBranches = branches.toArray(new String[0]);
			comboBox = new JComboBox(listBranches);
			comboBox.setBounds(5, 5, 541, 33);
			comboBox.addActionListener(this);
			brancheChoosen = comboBox.getSelectedItem().toString();
			contentPane.setLayout(null);
			contentPane.add(comboBox);
		} catch (GitGudExceptions e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		
		JPanel buttonreturnPanel = new JPanel();
		buttonreturnPanel.setBounds(551, 5, 427, 33);
		contentPane.add(buttonreturnPanel);
		
		returnButton = new JButton("");
		returnButton.setBounds(0, 0, 427, 33);
		returnButton.setIcon(new ImageIcon(ProjectView.class.getResource("/icon/return.png")));
		returnButton.addActionListener(this);
		buttonreturnPanel.setLayout(null);
		buttonreturnPanel.add(returnButton);
		
		panelButton = new JPanel();
		panelButton.setBounds(5, 38, 541, 617);
		contentPane.add(panelButton);
		panelButton.setLayout(new BoxLayout(panelButton, BoxLayout.Y_AXIS));
		
		JPanel panel = new JPanel();
		panel.setBounds(551, 38, 427, 617);
		panel.setLayout(null);
		contentPane.add(panel);
		
		JLabel labelStats = new JLabel("Statistics", SwingConstants.CENTER);
		labelStats.setBounds(0, 0, 427, 24);
		panel.add(labelStats);
		
		usersPanel = new JPanel();
		usersPanel.setBounds(10, 355, 407, 251);
		panel.add(usersPanel);
		usersPanel.setLayout(new BoxLayout(usersPanel, BoxLayout.Y_AXIS));
		
		JLabel labelUsers = new JLabel("Users", SwingConstants.CENTER);
		labelUsers.setBounds(10, 320, 397, 24);
		panel.add(labelUsers);
		
		statsPanel = new JPanel();
		statsPanel.setBounds(10, 23, 407, 251);
	    panel.add(statsPanel);
		
	    //Fonction permettant d'affichier les dossiers, fichiers et statistiques
		displayRepository();
		displayUsers();
		displayStatistic();
	
	}
	
	
	/** 
	 * @throws GitLabApiException
	 */
	public void displayRepository() {
		int i = 0;
		try {
			//On récupère les fichiers et dossiers depuis GitLab
			List<String> folder = controler.getFolder(id, brancheChoosen);
			List<String> file = controler.getFile(id, brancheChoosen);
			
			//Création de tableaux avec les boutons pour les dossiers et le texte pour les fichiers
			btns_list = new JButton[folder.size()];
			dossier = new String[folder.size()];
			
			for (String F : folder) {
				//Pour chaque dossier on crée un JButton
				JButton dossier_btn = new JButton(F.toString() +"/");
				btns_list[i] = dossier_btn;
				dossier[i] = F.toString();
				dossier_btn.addActionListener(this);
				panelButton.add(dossier_btn);
				i++;
			}
			
			for (String f : file) {
				//Pour chaque fichier on crée un JPanel
				JLabel file_lbl = new JLabel(f.toString());
				panelButton.add(file_lbl);
			}
		} catch (GitGudExceptions e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}

	}
	
	
	/** 
	 * @param path
	 * @throws GitLabApiException
	 */
	//Surchage de la méthode displayRepository, on rajoute un chemin quand nous ne sommes pas à la racine du projet
	public void displayRepository(String path) {
		int i = 0;
		try {
			//Même fonctionnalité que la fonction displayRepository() avec l'ajout du chemin permettant de récupérer les fichiers et dossiers
			List<String> file = controler.getFile(id, path, brancheChoosen);
			List<String> folder;
			folder = controler.getFolder(id, path, brancheChoosen);
			btns_list = new JButton[folder.size()];
			dossier = new String[folder.size()];
			
			for (String F : folder) {
				JButton dossier_btn = new JButton(F.toString() +"/");
				btns_list[i] = dossier_btn;
				dossier[i] = F.toString();
				dossier_btn.addActionListener(this);
				panelButton.add(dossier_btn);
				i++;
			}
			
			for (String f : file) {
				JLabel file_lbl = new JLabel(f.toString());
				panelButton.add(file_lbl);
			}
		} catch (GitGudExceptions e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	
	
	/** 
	 * @throws GitLabApiException
	 */
	public void displayStatistic() {
		statsPanel.setLayout(new BoxLayout(statsPanel, BoxLayout.Y_AXIS));
		//Visibility
		try {
			String visible;
			visible = controler.getVisibility(indexProject);
			JLabel visibility = new JLabel ("Visibility : " + visible);
			statsPanel.add(visibility);
		} catch (GitGudExceptions e) {
			e.printStackTrace();
		}
		
		
		//Nombre de branche
		JLabel branches = new JLabel("Branches : " + numberBranches);
		statsPanel.add(branches);
		
		//Nombre de commits
		try {
			JLabel commits;
			commits = new JLabel("Number of commits : " + controler.getNumberOfCommit(id, brancheChoosen));
			statsPanel.add(commits);
		} catch (GitGudExceptions e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		
		
		//Dernier commit
		try {
			Commit last_commit;
			last_commit = controler.getLastCommit(id, brancheChoosen);
			JLabel lastCommit = new JLabel("Last commit : ");
			statsPanel.add(lastCommit);
			JLabel lastCommitAuthor = new JLabel("   - Author : " + last_commit.getAuthorName());
			statsPanel.add(lastCommitAuthor);
			JLabel lastCommitDate = new JLabel("   - Date : " + last_commit.getCreatedAt().toString());
			statsPanel.add(lastCommitDate);
		} catch (GitGudExceptions e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		
		//Open Issues Count
		try {
			int issues;
			issues = controler.getOpenIssuesCount(indexProject);
			JLabel openIssues = new JLabel("Open issues : " + issues);
			statsPanel.add(openIssues);
		} catch (GitGudExceptions e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		
		//Project languages
		try {
			Map<String, Float> language;
			language = controler.getProjectLanguages(id);
			String listLanguage = " ";
			 for (Map.Entry mapentry : language.entrySet()) {
				 listLanguage = listLanguage + "  " + mapentry.getKey().toString();
			 }
			 JLabel languagesUsed = new JLabel("Language(s) : " + listLanguage);
			 statsPanel.add(languagesUsed);
		} catch (GitGudExceptions e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		
		//Web url
		try {
			final String url;
			url = controler.getWebUrl(indexProject);
			final JLabel webUrl = new JLabel (url);
			webUrl.setForeground(Color.BLUE.darker());
			webUrl.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			//Ajout d'un lien cliquable qui renvoie sur gitLab
			webUrl.addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent e) {
					try {
						Desktop.getDesktop().browse(new URI(url));
					} catch (IOException | URISyntaxException e1) {
						e1.printStackTrace();
					}
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
				}

				@Override
				public void mouseExited(MouseEvent e) {
					webUrl.setText(url);
				}

				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
			});
			statsPanel.add(webUrl);
		} catch (GitGudExceptions e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}

	}

	
	/** 
	 * @throws GitLabApiException
	 */
	public void displayUsers() {
		//On récupère les membres du projet et on les affiche
		try {
			List<Member> users = new ArrayList<>();
			users = controler.getUsers(id);
			for (Member u : users) {
				JLabel user_label = new JLabel(u.getUsername());
				usersPanel.add(user_label);
			}
		} catch (GitGudExceptions e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}

	
	
	/** 
	 * @param e
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		//Gestion des événements sur la fenêtre
		Object src = e.getSource();
		
		if (src == comboBox) {
			//Si on change de branche, on change le répertoire et les commits
			brancheChoosen = comboBox.getSelectedItem().toString();
			panelButton.removeAll();
			panelButton.updateUI();
			displayRepository();
			
			statsPanel.removeAll();
			statsPanel.updateUI();
			displayStatistic();
		} else if (src == returnButton) {
			panelButton.removeAll();
			panelButton.updateUI();
			//Pour retourner en arrière, on split le chemin pour pouvoir enlever deux éléments
			String[] tokens = beforeState.split("/");
			beforeState = "";
			for (int i = 0; i <tokens.length - 1;i++) {
				beforeState = beforeState + tokens[i] + "/";
			}
			displayRepository(beforeState);
		}
		
		for (int i = 0; i < btns_list.length; i++) {
			if (src == btns_list[i]) {
				beforeState = dossier[i];
				panelButton.removeAll();
				panelButton.updateUI();
				displayRepository(dossier[i]);
			}
		}

	}
}
