package com.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.controller.ConnexionController;
import com.controller.GitGudExceptions;
import com.model.ConnexionModel;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class EditURL extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField editUrlField;
	private JButton saveButton;
	private JButton cancelButton;

    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	
	/** 
	 * @param listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}


	/**
	 * Create the frame.
	 */
	public EditURL() {
		String gitURL = ConnexionModel.getGitlab_default_URL();
        setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(EditURL.class.getResource("/icon/sigle_telecom.png")));
		setTitle("Edit Default Gitlab URL");
		setBounds(100, 100, 700, 300);
        setIconImage(Toolkit.getDefaultToolkit().getImage(CreateAccount.class.getResource("/icon/sigle_telecom.png")));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		cancelButton = new JButton("Cancel");
		cancelButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cancelButton.setBounds(197, 183, 88, 31);
		cancelButton.addActionListener(this);
		contentPane.add(cancelButton);
		
		saveButton = new JButton("Save");
		saveButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		saveButton.setBounds(418, 183, 88, 31);
		saveButton.addActionListener(this);
		contentPane.add(saveButton);
		
		editUrlField = new JTextField();
		editUrlField.setHorizontalAlignment(SwingConstants.CENTER);
		editUrlField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		editUrlField.setColumns(10);
		editUrlField.setBounds(132, 109, 416, 34);
		editUrlField.setText(gitURL);
		contentPane.add(editUrlField);
		
		JLabel editUrlLabel = new JLabel("Edit default Gitlab URL :");
		editUrlLabel.setHorizontalAlignment(SwingConstants.CENTER);
		editUrlLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		editUrlLabel.setBounds(132, 38, 416, 48);
		contentPane.add(editUrlLabel);
	}


	
	/** 
	 * @param e
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();

		// Save new URL
		if (src == saveButton) {
			String newURL = editUrlField.getText();
			try {
				ConnexionController.setGitlabDefaultURL(newURL);
				JOptionPane d = new JOptionPane();
				d.showConfirmDialog(this, "New URL saved", 
				      "URL", JOptionPane.PLAIN_MESSAGE);
				this.dispose();
			} catch (GitGudExceptions e1) {
				JOptionPane.showMessageDialog(null, e1.getMessage());
			}
		}
		
		if (src == cancelButton) {
			this.dispose();
		}
		
	}


}