package com.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.LookAndFeel;

import com.controller.ConnexionController;
import com.controller.GitGudExceptions;
import com.formdev.flatlaf.FlatLightLaf;

import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import com.formdev.flatlaf.FlatDarkLaf;

import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.awt.event.ActionEvent;

public class ConnectionView extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPasswordField gitPasswordField;
	private JPasswordField localPasswordField;
	private JTextField gitUsernameField;
	private JTextField localUsernameField;
	private JButton btnConnectGit;
	private JButton btnConnectLocal;
	private JCheckBox checkBoxGit;
	private JCheckBox checkBoxLocal;
	private JTextField urlField;
	private JMenuItem modeClair;
	private JMenuItem darkMode;
	private LookAndFeel light;
	private LookAndFeel dark;


	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);     

	/** 
	 * @param listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {      
		this.pcs.addPropertyChangeListener(listener);
	}


	/**
	 * Create the frame.
	 */
	public ConnectionView() {	
		String gitURL = ConnexionController.getGitlabDefaultURL();
		String[] gitUserInfos = ConnexionController.getLastGitlabConnection();
		String[] localUserInfos = ConnexionController.getLastLocalConnection();
		if (! gitUserInfos[1].equals("")){
			gitURL = gitUserInfos[1];
		}

		light = new FlatLightLaf();
		dark = new FlatDarkLaf();

		try {
			UIManager.setLookAndFeel( light );
		} catch( Exception ex ) {
			JOptionPane.showMessageDialog(null, "Failed to initialize LaF","Error",JOptionPane.ERROR_MESSAGE,null );
			System.err.println( "Failed to initialize LaF" );
		}
		SwingUtilities.updateComponentTreeUI(getRootPane());

		setLocationRelativeTo(null);
		setTitle("Welcome");
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(ConnectionView.class.getResource("/icon/sigle_telecom.png")));
		setBounds(100, 100, 800, 550);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JMenuBar menubar = new JMenuBar();
		JMenu menu = new JMenu("File");

		// Créer les éléments du menu et sous menu
		JMenuItem edit = new JMenuItem("Edit default Gitlab URL");

		// Ajouter les éléments au menu
		menu.add(edit); 

		// Ajouter le menu au barre de menu
		menubar.add(menu);

		// Ajouter la barre de menu au frame
		setJMenuBar(menubar);

		JMenu menuMode = new JMenu("Mode");
		menubar.add(menuMode);

		modeClair = new JMenuItem("Light view");
		modeClair.addActionListener(this);
		menuMode.add(modeClair);

		darkMode = new JMenuItem("Dark view");
		darkMode.addActionListener(this);
		menuMode.add(darkMode);

		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				EditURL EditWindow = new EditURL();
				EditWindow.setVisible(true);
			}
		});

		JLabel connexionLabel = new JLabel("CONNECTION");
		connexionLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		connexionLabel.setHorizontalAlignment(SwingConstants.CENTER);
		connexionLabel.setBounds(273, 11, 210, 38);
		contentPane.add(connexionLabel);

		JLabel gitLabel = new JLabel("Git");
		gitLabel.setHorizontalAlignment(SwingConstants.CENTER);
		gitLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		gitLabel.setBounds(132, 66, 99, 22);
		contentPane.add(gitLabel);

		JLabel lblLocal = new JLabel("Local");
		lblLocal.setHorizontalAlignment(SwingConstants.CENTER);
		lblLocal.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblLocal.setBounds(546, 66, 99, 22);
		contentPane.add(lblLocal);

		checkBoxGit = new JCheckBox("Remember me");
		checkBoxGit.setFont(new Font("Tahoma", Font.PLAIN, 14));
		checkBoxGit.setBounds(119, 346, 115, 23);
		if(gitUserInfos[0].equals(""))
			checkBoxGit.setSelected(false);
		else
			checkBoxGit.setSelected(true);
		contentPane.add(checkBoxGit);

		checkBoxLocal = new JCheckBox("Remember me");
		checkBoxLocal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		checkBoxLocal.setBounds(530, 346, 115, 23);
		if(localUserInfos[0].equals(""))
			checkBoxLocal.setSelected(false);
		else
			checkBoxLocal.setSelected(true);
		contentPane.add(checkBoxLocal);

		btnConnectGit = new JButton("Connect");
		btnConnectGit.addActionListener(this);

		btnConnectGit.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnConnectGit.setBounds(118, 388, 131, 23);
		contentPane.add(btnConnectGit);

		btnConnectLocal = new JButton("Connect");
		btnConnectLocal.addActionListener(this);

		btnConnectLocal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnConnectLocal.setBounds(530, 388, 131, 23);
		contentPane.add(btnConnectLocal);

		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(381, 51, 9, 377);
		contentPane.add(separator);

		JPanel panel_git = new JPanel();
		panel_git.setBounds(54, 118, 268, 221);
		contentPane.add(panel_git);
		panel_git.setLayout(null);

		gitUsernameField = new JTextField();
		gitUsernameField.setBounds(36, 36, 180, 30);
		panel_git.add(gitUsernameField);
		gitUsernameField.setHorizontalAlignment(SwingConstants.CENTER);
		gitUsernameField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		gitUsernameField.setText(gitUserInfos[0]);
		gitUsernameField.setColumns(10);

		JLabel usernameGitLabel = new JLabel("Username");
		usernameGitLabel.setBounds(96, 8, 61, 17);
		panel_git.add(usernameGitLabel);
		usernameGitLabel.setHorizontalAlignment(SwingConstants.CENTER);
		usernameGitLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));

		JLabel passwordGitLabel = new JLabel("Password");
		passwordGitLabel.setBounds(81, 73, 90, 22);
		panel_git.add(passwordGitLabel);
		passwordGitLabel.setHorizontalAlignment(SwingConstants.CENTER);
		passwordGitLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));

		gitPasswordField = new JPasswordField();
		gitPasswordField.setBounds(36, 106, 180, 30);
		panel_git.add(gitPasswordField);
		gitPasswordField.setHorizontalAlignment(SwingConstants.CENTER);
		gitPasswordField.setFont(new Font("Tahoma", Font.PLAIN, 14));

		JLabel urlLabel = new JLabel("URL");
		urlLabel.setBounds(81, 147, 90, 22);
		panel_git.add(urlLabel);
		urlLabel.setHorizontalAlignment(SwingConstants.CENTER);
		urlLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));

		urlField = new JTextField();
		urlField.setBounds(36, 180, 180, 30);
		panel_git.add(urlField);
		urlField.setHorizontalAlignment(SwingConstants.CENTER);
		urlField.setFont(new Font("Tahoma", Font.PLAIN, 12));
		urlField.setColumns(10);
		urlField.setText(gitURL);

		JPanel panel_local = new JPanel();
		panel_local.setBounds(449, 143, 287, 163);
		contentPane.add(panel_local);
		panel_local.setLayout(null);

		JLabel usernameLocalLabel = new JLabel("Username");
		usernameLocalLabel.setBounds(113, 5, 61, 17);
		panel_local.add(usernameLocalLabel);
		usernameLocalLabel.setHorizontalAlignment(SwingConstants.CENTER);
		usernameLocalLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));

		localUsernameField = new JTextField();
		localUsernameField.setBounds(52, 35, 180, 30);
		panel_local.add(localUsernameField);
		localUsernameField.setHorizontalAlignment(SwingConstants.CENTER);
		localUsernameField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		localUsernameField.setColumns(10);
		localUsernameField.setText(localUserInfos[0]);

		JLabel passwordLocalLabel = new JLabel("Password");
		passwordLocalLabel.setBounds(101, 92, 90, 22);
		panel_local.add(passwordLocalLabel);
		passwordLocalLabel.setHorizontalAlignment(SwingConstants.CENTER);
		passwordLocalLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));

		localPasswordField = new JPasswordField();
		localPasswordField.setBounds(52, 122, 180, 30);
		panel_local.add(localPasswordField);
		localPasswordField.setHorizontalAlignment(SwingConstants.CENTER);
		localPasswordField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		localPasswordField.setText(localUserInfos[1]);
	}



	/** 
	 * Processes user actions on interface buttons
	 * @param e
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

		Object src = e.getSource();

		// Connexion Git
		if (src == btnConnectGit) {
			String idGit = gitUsernameField.getText();
			String pswGit = String.valueOf(gitPasswordField.getPassword());
			String urlGit = urlField.getText();

			try {
				// Enregistrer le username
				if(ConnexionController.gitlabConnexion(idGit, pswGit, urlGit)) {
					if(checkBoxGit.isSelected()) {
						ConnexionController.saveLastGitConnection(idGit, urlGit);
					}
					else{
						ConnexionController.saveLastGitConnection("", "");
					}
				}
				//CHANGEMENT DE FENETRE CONNEXION OK

				pcs.firePropertyChange("Connected", false, true);
				JOptionPane.showMessageDialog(null, 
						"Connected", 
						"Connexion", 
						JOptionPane.INFORMATION_MESSAGE, 
						null);


			} catch (GitGudExceptions e1) {
				JOptionPane.showMessageDialog(null, e1.getMessage(),"Error",JOptionPane.ERROR_MESSAGE,null );
			}
		}

		// Connexion locale
		if (src == btnConnectLocal) {
			String idLocal = localUsernameField.getText();
			String pswLocal = String.valueOf(this.localPasswordField.getPassword());

			try {
				// Vérification de la connexion
				if(ConnexionController.localConnection(idLocal, pswLocal)) {
					if(checkBoxLocal.isSelected()) {
						ConnexionController.saveLastLocalConnection(idLocal, pswLocal);
					}
					else{
						ConnexionController.saveLastLocalConnection("", "");
					}

					// CHANGEMENT DE FENETRE CONNEXION OK
					pcs.firePropertyChange("Connected", false, true);


				}
			}
			catch (GitGudExceptions e1) {
				JOptionPane.showMessageDialog(null, e1.getMessage(),"Error",JOptionPane.ERROR_MESSAGE,null );
			}
		}

		//Mode clair
		if (src == modeClair) {

			try {
				UIManager.setLookAndFeel(light);
			} catch( Exception ex ) {
				JOptionPane.showMessageDialog(null, "Failed to initialize LaF","Error",JOptionPane.ERROR_MESSAGE,null );
				System.err.println( "Failed to initialize LaF" );
			}
			SwingUtilities.updateComponentTreeUI(getRootPane());
		}

		//Mode Sombre
		if (src == darkMode) {
			try {
				UIManager.setLookAndFeel(dark);
			} catch( Exception ex ) {
				JOptionPane.showMessageDialog(null, "Failed to initialize LaF","Error",JOptionPane.ERROR_MESSAGE,null );
				System.err.println( "Failed to initialize LaF" );
			}
			SwingUtilities.updateComponentTreeUI(getRootPane());
		}
	}
}

