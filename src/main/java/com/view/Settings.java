package com.view;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.controller.ConnexionController;
import com.controller.GitGudExceptions;
import com.model.ConnexionModel;

public class Settings extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField newUsernameField;
	private JPasswordField newPasswordField;
	private JPasswordField newConfirmField;
	private JTextField newUrlField;
	private JPasswordField newTokenField;
	private JButton saveButton;
	private JButton cancelButton;

	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	
	/** 
	 * @param listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	/**
	 * Create the frame
	 */
	public Settings() {
		
        setResizable(false);
		setTitle("Settings");
		setIconImage(Toolkit.getDefaultToolkit().getImage(CreateAccount.class.getResource("/icon/sigle_telecom.png")));
		setBounds(100, 100, 700, 500);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel accountLabel = new JLabel("Change your account informations");
		accountLabel.setBounds(166, 11, 372, 41);
		accountLabel.setHorizontalAlignment(SwingConstants.CENTER);
		accountLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		
		JLabel newUsernameLabel = new JLabel("Edit Username :");
		newUsernameLabel.setBounds(142, 87, 179, 28);
		newUsernameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		newUsernameLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel newPasswordLabel = new JLabel("Edit Password :");
		newPasswordLabel.setBounds(142, 144, 179, 28);
		newPasswordLabel.setHorizontalAlignment(SwingConstants.CENTER);
		newPasswordLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel newConfirmLabel = new JLabel("Confirm your new password :");
		newConfirmLabel.setBounds(110, 201, 211, 28);
		newConfirmLabel.setHorizontalAlignment(SwingConstants.CENTER);
		newConfirmLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel newUrlLabel = new JLabel("Edit URL :");
		newUrlLabel.setBounds(142, 258, 179, 28);
		newUrlLabel.setHorizontalAlignment(SwingConstants.CENTER);
		newUrlLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel newTokenLabel = new JLabel("Edit Token :");
		newTokenLabel.setBounds(142, 316, 179, 28);
		newTokenLabel.setHorizontalAlignment(SwingConstants.CENTER);
		newTokenLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		newUsernameField = new JTextField();
		newUsernameField.setBounds(331, 87, 259, 28);
		newUsernameField.setHorizontalAlignment(SwingConstants.CENTER);
		newUsernameField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		newUsernameField.setColumns(10);
		
		newPasswordField = new JPasswordField();
		newPasswordField.setBounds(331, 144, 259, 28);
		newPasswordField.setHorizontalAlignment(SwingConstants.CENTER);
		newPasswordField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		newConfirmField = new JPasswordField();
		newConfirmField.setBounds(331, 201, 259, 28);
		newConfirmField.setHorizontalAlignment(SwingConstants.CENTER);
		newConfirmField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		newUrlField = new JTextField();
		newUrlField.setBounds(331, 258, 259, 28);
		newUrlField.setHorizontalAlignment(SwingConstants.CENTER);
		newUrlField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		newUrlField.setColumns(10);
		
		newTokenField = new JPasswordField();
		newTokenField.setBounds(331, 316, 259, 28);
		newTokenField.setHorizontalAlignment(SwingConstants.CENTER);
		newTokenField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		cancelButton = new JButton("Cancel");
		cancelButton.setBounds(233, 406, 88, 31);
		cancelButton.addActionListener(this);
		cancelButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		saveButton = new JButton("Save");
		saveButton.setBounds(402, 406, 88, 31);
		saveButton.addActionListener(this);
		saveButton.setFont(new Font("Tahoma", Font.PLAIN, 14));

		contentPane.setLayout(null);
		contentPane.add(accountLabel);
		contentPane.add(newUsernameLabel);
		contentPane.add(newPasswordLabel);
		contentPane.add(newConfirmLabel);
		contentPane.add(newUrlLabel);
		contentPane.add(newTokenLabel);
		
		try {
			Map<String, String> profilSettings = ConnexionController.getProfilSettings();
			newUsernameField.setText(ConnexionModel.getUsername());
			newPasswordField.setText(profilSettings.get("password"));
			newConfirmField.setText(profilSettings.get("password"));
			newUrlField.setText(profilSettings.get("url"));
			newTokenField.setText(profilSettings.get("token"));
		} catch (GitGudExceptions e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			this.dispose();
		}
		
		contentPane.add(newUsernameField);
		contentPane.add(newPasswordField);
		contentPane.add(newConfirmField);
		contentPane.add(newUrlField);
		contentPane.add(newTokenField);
		contentPane.add(cancelButton);
		contentPane.add(saveButton);
	}

	
	/** 
	 * @param e
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();

		// Save new user infos
		if (src == saveButton) {
			String newUsername = newUsernameField.getText();
			String newPsw = String.valueOf(newPasswordField.getPassword());
			String newPswConf = String.valueOf(newConfirmField.getPassword());
			String newURL = newUrlField.getText();
			String newToken = String.valueOf(newTokenField.getPassword());
			try {
				ConnexionController.editProfilSettings(newUsername, newPsw, newPswConf, newURL, newToken);
				JOptionPane.showConfirmDialog(this, "Saved",  "Save", JOptionPane.PLAIN_MESSAGE);
				this.dispose();
			} catch (GitGudExceptions e1) {
				JOptionPane.showMessageDialog(null, e1.getMessage());
			}
		}
		
		if (src == cancelButton) {
			this.dispose();
		}
		
	}
}