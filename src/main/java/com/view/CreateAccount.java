package com.view;

import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.gitlab4j.api.Constants.TokenType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.controller.ConnexionController;
import com.controller.GitGudExceptions;
import com.model.AES;
import com.model.ConnexionModel;

public class CreateAccount extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private JPanel contentPane;
	private JPasswordField passwordField;
	private JPasswordField confirmField;
	private JPasswordField tokenField;
	private JTextField usernameField;
	private JTextField urlField;
	private JButton saveButton;
	private JButton cancelButton;

	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	
	/** 
	 * @param listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	/**
	 * Create the frame.
	 */
	public CreateAccount() {

        setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 700, 500);
        setTitle("Create Account");
		setIconImage(Toolkit.getDefaultToolkit().getImage(CreateAccount.class.getResource("/icon/sigle_telecom.png")));
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel accountLabel = new JLabel("Let's create your account");
		accountLabel.setBounds(187, 11, 325, 41);
		accountLabel.setHorizontalAlignment(SwingConstants.CENTER);
		accountLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		
		JLabel usernameLabel = new JLabel("Username :");
		usernameLabel.setBounds(142, 87, 179, 28);
		usernameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		usernameLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel passwordLabel = new JLabel("Password :");
		passwordLabel.setBounds(142, 144, 179, 28);
		passwordLabel.setHorizontalAlignment(SwingConstants.CENTER);
		passwordLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel confirmLabel = new JLabel("Confirm your password :");
		confirmLabel.setBounds(142, 201, 179, 28);
		confirmLabel.setHorizontalAlignment(SwingConstants.CENTER);
		confirmLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel urlLabel = new JLabel("URL :");
		urlLabel.setBounds(142, 258, 179, 28);
		urlLabel.setHorizontalAlignment(SwingConstants.CENTER);
		urlLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel tokenLabel = new JLabel("Token :");
		tokenLabel.setBounds(142, 316, 179, 28);
		tokenLabel.setHorizontalAlignment(SwingConstants.CENTER);
		tokenLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		usernameField = new JTextField();
		usernameField.setBounds(331, 87, 259, 28);
		usernameField.setHorizontalAlignment(SwingConstants.CENTER);
		usernameField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		usernameField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(331, 144, 259, 28);
		passwordField.setHorizontalAlignment(SwingConstants.CENTER);
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		confirmField = new JPasswordField();
		confirmField.setBounds(331, 201, 259, 28);
		confirmField.setHorizontalAlignment(SwingConstants.CENTER);
		confirmField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		urlField = new JTextField();
		urlField.setBounds(331, 258, 259, 28);
		urlField.setHorizontalAlignment(SwingConstants.CENTER);
		urlField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		urlField.setText(ConnexionModel.getGitlab_default_URL());
		urlField.setColumns(10);
		
		tokenField = new JPasswordField();
		tokenField.setBounds(331, 316, 259, 28);
		tokenField.setHorizontalAlignment(SwingConstants.CENTER);
		tokenField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		cancelButton = new JButton("Cancel");
		cancelButton.setBounds(233, 389, 88, 31);
		cancelButton.addActionListener(this);
		cancelButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		saveButton = new JButton("Save");
		saveButton.setBounds(400, 389, 88, 31);
		saveButton.addActionListener(this);
		saveButton.setFont(new Font("Tahoma", Font.PLAIN, 14));

		contentPane.setLayout(null);
		contentPane.add(accountLabel);
		contentPane.add(usernameLabel);
		contentPane.add(passwordLabel);
		contentPane.add(confirmLabel);
		contentPane.add(urlLabel);
		contentPane.add(tokenLabel);
		contentPane.add(usernameField);
		contentPane.add(passwordField);
		contentPane.add(confirmField);
		contentPane.add(urlField);
		contentPane.add(tokenField);
		contentPane.add(cancelButton);
		contentPane.add(saveButton);
	}

	
	/** 
	 * Processes user actions on interface buttons
	 * @param e
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();

		if(src == cancelButton) {
			
		}
		
		// Création d'un compte
		if (src == saveButton) {
			boolean verif = false;
			String username = usernameField.getText();
			String password =  String.valueOf(passwordField.getPassword());
			String passwordConf = String.valueOf(confirmField.getPassword());
			String url = urlField.getText();
			String token = String.valueOf(tokenField.getPassword());
			
			//Vérifier que les champs ne sont pas vides
			
			try {
				if (ConnexionController.createAccount(username, password, passwordConf, url, token)) {
					JOptionPane.showConfirmDialog(this, "Account created.", "Save", JOptionPane.PLAIN_MESSAGE);
					this.dispose();
				}
			} catch (GitGudExceptions e1) {
				JOptionPane.showMessageDialog(null, e1.getMessage());
			}
		}
		
		if (src == cancelButton) {
			this.dispose();
		}
	}
}

