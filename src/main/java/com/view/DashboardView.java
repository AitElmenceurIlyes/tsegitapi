package com.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JFileChooser;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.filechooser.FileNameExtensionFilter;

import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.AccessLevel;
import org.gitlab4j.api.models.Group;

import com.controller.DashboardController;
import com.controller.GitGudExceptions;
import com.controller.ProjectController;
import com.model.ConnexionModel;
import com.model.Module;
import com.model.MyGroups;
import com.model.MyModules;
import com.model.MyProjects;
import com.model.Project;
import com.model.UserGit;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.LookAndFeel;

import java.awt.CardLayout;
import java.awt.Cursor;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.Component;
import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLightLaf;
import java.awt.event.KeyListener;

import javax.swing.JCheckBox;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;



@SuppressWarnings("serial")
public class DashboardView extends JFrame implements PropertyChangeListener,ActionListener,ChangeListener,KeyListener{
	

  protected ProjectController pjController;
  protected DashboardController dashboardController;
  protected MyProjects myProjects;
  protected MyGroups myGroups;
  protected MyModules myModules;
//    protected GroupModel grModel;
//    protected GroupController grController;
  JFileChooser fc;
  JOptionPane d = new JOptionPane();
  boolean isShiftPressed = false;
  
  int lastSelectedProject = -1;
  int lastUnselectedProject = -1;

  int lastSelectedGroup = -1;
  int lastUnselectedGroup = -1;

  int lastSelectedModule = -1;
  int lastUnselectedModule = -1;

    
    class DashElement{
    	JButton btn;
    	int id;
    	JCheckBox checkBox;
    	DashElement(JButton btn, int id, JCheckBox checkBox){ 
    		this.btn = btn;
    		this.id = id;
        	this.checkBox = checkBox;
    	}
    }

    private DashElement[] myProjectsList;
    private JPanel myProjectsPanel;
    private JPanel myProjectsPanelChild;

	
    private DashElement[] myGroupsList;
    private JPanel myGroupsPanel;
    private JPanel myGroupsPanelChild;
    private JButton previousBtn;
    private Integer parentGroupId = null;
	
    private DashElement[] myModulesList;
    private JPanel myModulesPanel;
    private JPanel myModulesPanelChild;

    private JTabbedPane dashboardTabbedPane;

    private JMenuItem addFromCSVMenuItem;
    private JMenuItem addProjectsToGroupMenuItem;
    private JMenuItem deleteProjectsMenuItem;
    private JMenuItem archiveMenu;
    private JMenuItem unarchiveMenu;
    JComboBox<String> archiveProjectComboBox;

    private JMenuItem addGroupstoModuleMenuItem;
    private JMenuItem deleteGroupsMenuItem;
	
    private JMenuItem addModulesMenuItem;
    private JMenuItem archiveModulesMenuItem;
    private JMenuItem deleteModulesMenuItem;
    JComboBox<String> archiveModuleComboBox;
	
    private JMenuItem projectLightModeMenuItem;
    private JMenuItem projectDarkModeMenuItem;

    
    private JMenuItem groupLightModeMenuItem;
    private JMenuItem groupDarkModeMenuItem;
    
    private JMenuItem moduleLightModeMenuItem;
    private JMenuItem moduleDarkModeMenuItem;
	
	private LookAndFeel light;
	private LookAndFeel dark;
	
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);     

	/** 
	 * @param listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {      
		this.pcs.addPropertyChangeListener(listener);
	}
	
	public DashboardView() { 
        myProjects = new MyProjects();
//      	myProjects.addPropertyChangeListener(this);
      	
      	myGroups = new MyGroups();
//      	myGroups.addPropertyChangeListener(this);
      	
      	myModules = new MyModules();
//      	myModules.addPropertyChangeListener(this);

        dashboardController = new DashboardController(myProjects);


		
		setTitle("Main Window");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(ConnectionView.class.getResource("/icon/sigle_telecom.png")));
		setSize(1200,800);
		setLocationRelativeTo(null);
		
		light = new FlatLightLaf();
		dark = new FlatDarkLaf();
		
		fc = new JFileChooser();
		fc.setCurrentDirectory(new File(System.getProperty("user.home")));
		fc.setAcceptAllFileFilterUsed(false);
		fc.addChoosableFileFilter(new FileNameExtensionFilter("CSV Documents", "csv"));
		getContentPane().setLayout(new CardLayout(0, 0));
		
		dashboardTabbedPane = new JTabbedPane(JTabbedPane.TOP);
		getContentPane().add(dashboardTabbedPane);
		
		
		myProjectsPanel = new JPanel();
		dashboardTabbedPane.addTab("My Projects", null, myProjectsPanel, null);
		myProjectsPanel.setLayout(new BorderLayout(0, 0));
		
		JMenuBar myProjectsMenuBar = new JMenuBar();
		myProjectsMenuBar.setBorderPainted(false);
		myProjectsPanel.add(myProjectsMenuBar, BorderLayout.NORTH);
		
		JMenu projectsFileMenu = new JMenu("File");
		myProjectsMenuBar.add(projectsFileMenu);
		
		JMenu addProjectMenu = new JMenu("Add project(s)");
		projectsFileMenu.add(addProjectMenu);
		
		addFromCSVMenuItem = new JMenuItem("From CSV");
		addProjectMenu.add(addFromCSVMenuItem);
		
		archiveMenu = new JMenuItem("Archive selected project(s)");
		archiveMenu.addActionListener(this);
		projectsFileMenu.add(archiveMenu);
		
		unarchiveMenu = new JMenuItem("Unarchive selected project(s)");
		unarchiveMenu.addActionListener(this);
		projectsFileMenu.add(unarchiveMenu);
		addFromCSVMenuItem.addActionListener(this);
		
		JMenu projectsEditMenu = new JMenu("Edit");
		projectsEditMenu.setHorizontalAlignment(SwingConstants.LEFT);
		myProjectsMenuBar.add(projectsEditMenu);
		
		addProjectsToGroupMenuItem = new JMenuItem("Share selected project(s) to group");
		projectsEditMenu.add(addProjectsToGroupMenuItem);
		addProjectsToGroupMenuItem.addActionListener(this);

		
		deleteProjectsMenuItem = new JMenuItem("Delete selected project(s)");
		projectsEditMenu.add(deleteProjectsMenuItem);
		deleteProjectsMenuItem.addActionListener(this);

		JMenu myProjectsUserMenu = new JMenu(ConnexionModel.getUsername());
		myProjectsUserMenu.setHorizontalAlignment(SwingConstants.RIGHT);

		JMenu modeView_projects = new JMenu("Mode");
		modeView_projects.setHorizontalAlignment(SwingConstants.LEFT);
		
		projectLightModeMenuItem = new JMenuItem("Light");
		projectLightModeMenuItem.addActionListener(this);
		modeView_projects.add(projectLightModeMenuItem);
		
		projectDarkModeMenuItem = new JMenuItem("Dark");
		projectDarkModeMenuItem.addActionListener(this);
		modeView_projects.add(projectDarkModeMenuItem);
		myProjectsMenuBar.add(Box.createHorizontalGlue());

		String[] archiveProjectString = {"active project(s)", "archived project(s)"};
		archiveProjectComboBox = new JComboBox<String>(archiveProjectString);
		archiveProjectComboBox.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    	myProjects.update();
		    	displayProjects();
		    }
		});
		((JLabel)archiveProjectComboBox.getRenderer()).setHorizontalAlignment(JLabel.RIGHT);
		myProjectsMenuBar.add(archiveProjectComboBox);
		
		myProjectsMenuBar.add(modeView_projects);
		myProjectsMenuBar.add(myProjectsUserMenu);

	
		
		JMenuItem projectDisconnectMenuItem = new JMenuItem("Disconnect");
		myProjectsUserMenu.add(projectDisconnectMenuItem);
		projectDisconnectMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
	        	pcs.firePropertyChange("Disconnect", false, true);
			}
		});
		
		myProjectsPanelChild = new JPanel();
		myProjectsPanelChild.setLayout(new BoxLayout(myProjectsPanelChild, BoxLayout.Y_AXIS));
		
		JScrollPane myProjectsScrollPane = new JScrollPane(myProjectsPanelChild);
		myProjectsPanel.add(myProjectsScrollPane, BorderLayout.CENTER);


		
		myGroupsPanel = new JPanel();
		dashboardTabbedPane.addTab("My groups", null, myGroupsPanel, null);
		myGroupsPanel.setLayout(new BorderLayout(0, 0));
		
		JMenuBar myGroupsMenuBar = new JMenuBar();
		myGroupsMenuBar.setBorderPainted(false);
		myGroupsPanel.add(myGroupsMenuBar, BorderLayout.NORTH);
		
		JMenu myGroupsFileMenu = new JMenu("File");
		myGroupsMenuBar.add(myGroupsFileMenu);
		
//		JMenuItem addGroupMenuItem = new JMenuItem("Add a Group");
//		myGroupsFileMenu.add(addGroupMenuItem);
		
		JMenu myGroupsEditMenu = new JMenu("Edit");
		myGroupsMenuBar.add(myGroupsEditMenu);
		
		deleteGroupsMenuItem = new JMenuItem("Delete selected group(s)");
		myGroupsEditMenu.add(deleteGroupsMenuItem);
		deleteGroupsMenuItem.addActionListener(this);

		String[] archiveModuleString = {"active project(s)", "archived project(s)"};
		archiveModuleComboBox = new JComboBox<String>(archiveModuleString);
		archiveModuleComboBox.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    	myModules.update();
		    	displayModules();
		    }
		});
		((JLabel)archiveProjectComboBox.getRenderer()).setHorizontalAlignment(JLabel.RIGHT);

		
		JMenu myGroupsUserMenu = new JMenu(ConnexionModel.getUsername());
		myGroupsUserMenu.setHorizontalAlignment(SwingConstants.RIGHT);

		JMenu modeView_groups = new JMenu("Mode");
		modeView_groups.setHorizontalAlignment(SwingConstants.LEFT);
		
		groupLightModeMenuItem = new JMenuItem("Light");
		groupLightModeMenuItem.addActionListener(this);
		modeView_groups.add(groupLightModeMenuItem);
		
		groupDarkModeMenuItem = new JMenuItem("Dark");
		groupDarkModeMenuItem.addActionListener(this);
		modeView_groups.add(groupDarkModeMenuItem);
		myGroupsMenuBar.add(Box.createHorizontalGlue());
		myProjectsMenuBar.add(archiveProjectComboBox);
		myGroupsMenuBar.add(modeView_groups);
		myGroupsMenuBar.add(myGroupsUserMenu);
		
		
		
		JMenuItem groupDisconnectMenuItem = new JMenuItem("Disconnect");
		myGroupsUserMenu.add(groupDisconnectMenuItem);
		groupDisconnectMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
	        	pcs.firePropertyChange("Disconnect", false, true);
			}
		});


		myGroupsPanelChild = new JPanel();
		myGroupsPanelChild.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		myGroupsPanel.add(myGroupsPanelChild, BorderLayout.CENTER);
		myGroupsPanelChild.setLayout(new BoxLayout(myGroupsPanelChild, BoxLayout.Y_AXIS));
		
		JScrollPane myGroupsScrollPane = new JScrollPane(myGroupsPanelChild);
		myGroupsPanel.add(myGroupsScrollPane, BorderLayout.CENTER);
		
		if (ConnexionModel.getIsALocalUser()) {
			
			JMenuItem createAccountItem = new JMenuItem("Create account");
			createAccountItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e1) {
					CreateAccount CreateWindow = new CreateAccount();
					CreateWindow.setVisible(true);
				}
			});
			myProjectsUserMenu.add(createAccountItem);
			
			JMenuItem projectSettingsMenuItem = new JMenuItem("Settings");
			projectSettingsMenuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e1) {
					Settings SettingsWindow = new Settings();
					SettingsWindow.setVisible(true);
				}
			});
			myProjectsUserMenu.add(projectSettingsMenuItem);
			
			addGroupstoModuleMenuItem = new JMenuItem("Add selected group(s) to module");
			myGroupsEditMenu.add(addGroupstoModuleMenuItem);
			addGroupstoModuleMenuItem.addActionListener(this);

			JMenuItem createAccountItem_1 = new JMenuItem("Create account");
			createAccountItem_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e1) {
					CreateAccount CreateWindow = new CreateAccount();
					CreateWindow.setVisible(true);
				}
			});
			myGroupsUserMenu.add(createAccountItem_1);
			
			JMenuItem groupSettingsMenuItem = new JMenuItem("Settings");
			groupSettingsMenuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e1) {
					Settings SettingsWindow = new Settings();
					SettingsWindow.setVisible(true);
				}
			});
			myGroupsUserMenu.add(groupSettingsMenuItem);
			
			
			myModulesPanel = new JPanel();
			dashboardTabbedPane.addTab("My Modules", null, myModulesPanel, null);
			myModulesPanel.setLayout(new BorderLayout(0, 0));
			
			JMenuBar myModulesMenuBar = new JMenuBar();
			myModulesMenuBar.setBorderPainted(false);
			myModulesPanel.add(myModulesMenuBar, BorderLayout.NORTH);
			
			JMenu myModulesFileMenu = new JMenu("File");
			myModulesMenuBar.add(myModulesFileMenu);
			
			addModulesMenuItem = new JMenuItem("Add a Module");
			myModulesFileMenu.add(addModulesMenuItem);
			addModulesMenuItem.addActionListener(this);

			JMenu myModulesEditMenu = new JMenu("Edit");
			myModulesMenuBar.add(myModulesEditMenu);
			
			deleteModulesMenuItem = new JMenuItem("Delete selected module(s)");
			myModulesEditMenu.add(deleteModulesMenuItem);
			deleteModulesMenuItem.addActionListener(this);

			archiveModulesMenuItem = new JMenuItem("Archive selected module(s)");
			myModulesEditMenu.add(archiveModulesMenuItem);
			archiveModulesMenuItem.addActionListener(this);

			JMenu myModulesUserMenu = new JMenu(ConnexionModel.getUsername());
			
			JMenu modeView_modules = new JMenu("Mode");
			modeView_modules.setHorizontalAlignment(SwingConstants.LEFT);
			
			moduleLightModeMenuItem = new JMenuItem("Light");
			moduleLightModeMenuItem.addActionListener(this);
			modeView_modules.add(moduleLightModeMenuItem);
			
			moduleDarkModeMenuItem = new JMenuItem("Dark");
			moduleDarkModeMenuItem.addActionListener(this);
			modeView_modules.add(moduleDarkModeMenuItem);
			myModulesMenuBar.add(Box.createHorizontalGlue());
			myModulesMenuBar.add(modeView_modules);
			myModulesMenuBar.add(myModulesUserMenu);

			
			JMenuItem createAccountItem_2 = new JMenuItem("Create account");
			createAccountItem_2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e1) {
					CreateAccount CreateWindow = new CreateAccount();
					CreateWindow.setVisible(true);
				}
			});
			myModulesUserMenu.add(createAccountItem_2);
			
			JMenuItem moduleSettingsMenuItem = new JMenuItem("Settings");
			moduleSettingsMenuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e1) {
					Settings SettingsWindow = new Settings();
					SettingsWindow.setVisible(true);
				}
			});
			myModulesUserMenu.add(moduleSettingsMenuItem);
			
			JMenuItem moduleDisconnectMenuItem = new JMenuItem("Disconnect");
			myModulesUserMenu.add(moduleDisconnectMenuItem);
			moduleDisconnectMenuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e1) {
		        	pcs.firePropertyChange("Disconnect", false, true);
				}
			});

			
			myModulesPanelChild = new JPanel();
			myModulesPanel.add(myModulesPanelChild, BorderLayout.CENTER);
			myModulesPanelChild.setLayout(new BoxLayout(myModulesPanelChild, BoxLayout.Y_AXIS));
			
			JScrollPane myModulesScrollPane = new JScrollPane(myModulesPanelChild);
			myModulesPanel.add(myModulesScrollPane, BorderLayout.CENTER);
			
			
		}
		
		

	
		displayProjects();
		dashboardTabbedPane.addChangeListener(this);
		setVisible(true);
	}


	public void displayProjects() {
		int i = 0;
		try {
			dashboardController.getProjects((boolean) (archiveProjectComboBox.getSelectedIndex() == 1));
		} catch (GitGudExceptions e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE,null );
		}
		List<Project> myprojects = myProjects.getProjects();
		myProjectsPanelChild.removeAll();
		
		
		myProjectsList = new DashElement[myprojects.size()] ;
		for (org.gitlab4j.api.models.Project project : myprojects) {
	
			JButton pj_btn = new JButton(project.getNameWithNamespace().toString());
			pj_btn.addActionListener(this);
	        // add button to panel
			JPanel panel = new JPanel();
			myProjectsPanelChild.add(panel);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			panel.setAlignmentX(Component.LEFT_ALIGNMENT);

			JCheckBox checkBox = new JCheckBox("");
			checkBox.addActionListener(this);
			checkBox.addKeyListener(this);

			panel.add(checkBox, BorderLayout.WEST);
			panel.add(pj_btn);
			myProjectsList[i] = new DashElement(pj_btn,project.getId(),checkBox);
	        i++;
		}
		myProjectsPanelChild.revalidate();
		myProjectsPanelChild.repaint();
	}

	
	
	/** 
	 * @param myGroups
	 */
	public void displayGroups() {
		int i = 0;
		myGroupsPanelChild.removeAll();
		List<Group> newGroups = myGroups.getGroups(parentGroupId);
		myGroupsList = new DashElement[newGroups.size()] ;

		previousBtn = new JButton("");
		previousBtn.setIcon(new ImageIcon(ProjectView.class.getResource("/icon/return.png")));
		previousBtn.addActionListener(this);
		myGroupsPanelChild.add(previousBtn);
		


		for (Group group : newGroups) {
//			System.out.println("test : " + group.toString());
			JButton btn = new JButton("idgrp : "+ group.getId() + ". Name : " + group.getName()); // + group.getPathWithNamespace().toString());
	        btn.addActionListener(this);
	        
			JPanel panel = new JPanel();
			myGroupsPanelChild.add(panel);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			panel.setAlignmentX(Component.LEFT_ALIGNMENT);

			JCheckBox checkBox = new JCheckBox("");
			checkBox.addActionListener(this);
			checkBox.addKeyListener(this);
			panel.add(checkBox, BorderLayout.WEST);
			panel.add(btn);
	        
			myGroupsList[i] = new DashElement(btn,group.getId(),checkBox);

	        i++;
		}
		myGroupsPanelChild.revalidate();
		myGroupsPanelChild.repaint();
	}


	

	public void displayModules() {
		int i = 0;
		Module[] modules = myModules.getModules();
		int l = modules.length;
		
		myModulesPanelChild.removeAll();
	
		
		myModulesList = new DashElement[l] ;
//		System.out.println("idlist : " + id_list);
//		System.out.println("taille : " + l);
		for (Module module : modules) {
//			System.out.println("test : " + module.toString());
			JButton btn = new JButton("Name : " + module.getName()); // + group.getPathWithNamespace().toString());
	        btn.addActionListener(this);
	        // add button to panel
			JPanel panel = new JPanel();
			
			myModulesPanelChild.add(panel);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			panel.setAlignmentX(Component.LEFT_ALIGNMENT);

			JCheckBox checkBox = new JCheckBox("");
			panel.add(checkBox, BorderLayout.WEST);
			checkBox.addActionListener(this);
			checkBox.addKeyListener(this);

			panel.add(btn);
	        
			myModulesList[i] = new DashElement(btn,module.getId(),checkBox);
			
	        i++;
		}
		myModulesPanelChild.revalidate();
		myModulesPanelChild.repaint();
	}

	
	/** 
	 * @param evt
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
//		if(evt.getPropertyName() == "MyProjects") {
//			System.out.println("My Projects changed");
//		}
	}

	
    
	/** 
	 * @param e
	 */
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
        if (src == addFromCSVMenuItem) {
            int returnVal = fc.showOpenDialog(null);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File csv_file = fc.getSelectedFile();
                
                JTextArea csvText = new JTextArea(15,50);
		        csvText.setMargin(new Insets(5,5,5,5));
		        csvText.setEditable(false);
		        JScrollPane logScrollPane = new JScrollPane(csvText);
		        Map<String, ArrayList<Project>> groupList = null;
				try {
					groupList = myProjects.getProjectsFromCsv(csv_file);
				} catch (GitGudExceptions e2) {
	        		JOptionPane.showMessageDialog(null, "Error reading the file" + e2.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
				// Itération sur les groups
				for (Map.Entry<String, ArrayList<Project>> group : groupList.entrySet()) {
					csvText.append("================" + group.getKey() + "================\n");
					// Itération sur les projets
					for (Project project : group.getValue()) {
						csvText.append("-----------------" + project.getName() + "-----------------\n");
						// Itération sur les Users
						for (UserGit user : project.getUser()) {
							csvText.append(user.getUsername() + "\n");
						}
						
					}
					csvText.append("\n\n");
				}
				int confirm = JOptionPane.showConfirmDialog(null,
						logScrollPane, "Do you want to add these projects ?", JOptionPane.YES_NO_OPTION);

				if (confirm == 0) {
					String logs = "";
					try {
						logs = myProjects.addProjectsFromCsv(csv_file);
						System.out.println(logs);
					} catch (GitGudExceptions e1) {
		        		JOptionPane.showMessageDialog(null, "Error adding from CSV" + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
					if (logs == "") {					
						JOptionPane.showMessageDialog(null, 
					         "Projects Added", 
					         "Project creation completed", 
					         JOptionPane.INFORMATION_MESSAGE, 
					         null);
					} else {
		                JTextArea errorText = new JTextArea(15,50);
		                errorText.setMargin(new Insets(5,5,5,5));
		                errorText.setEditable(false);
				        JScrollPane errorScrollPane = new JScrollPane(errorText);
				        errorText.append("Some errors occured during process:\n\n"+logs);
						JOptionPane.showMessageDialog(null, 
						         errorScrollPane, 
						         "Project creation completed with errors", 
						         JOptionPane.WARNING_MESSAGE, 
						         null);
					}

					myProjects.update();
					displayProjects();
				}
            }
            
        } 
        
		else if (src == addProjectsToGroupMenuItem) {
			String[] accessLevels = {
					"GUEST",
					"REPORTER",
					"DEVELOPER",
					"MAINTAINER"};

			JComboBox<String> accesLevelsList = new JComboBox<String>(accessLevels);
			accesLevelsList.setSelectedIndex(2);    
			String[] groupNames = myGroups.getGroupsNames();
			JComboBox<String>  groupNamesList = new JComboBox<String>(groupNames) ;

			Object[] message = {
			    "Group:", groupNamesList,
			    "Acces Level:", accesLevelsList
			};

			int option = JOptionPane.showConfirmDialog(null, message, "Choose the Group and Access Level", JOptionPane.OK_CANCEL_OPTION);
			if (option == JOptionPane.OK_OPTION) {	
				int selectedGroupId = myGroups.getGroup(groupNamesList.getSelectedItem().toString()).getId();
				AccessLevel selectedAccessLevel = AccessLevel.valueOf(accesLevelsList.getSelectedItem().toString());			
		       	ArrayList<Integer> projectsToShareIds = new ArrayList<Integer>();
		       	JTextArea projectsToShareNames = new JTextArea(15,50);
		       	projectsToShareNames.setMargin(new Insets(5,5,5,5));
		       	projectsToShareNames.setEditable(false);
		        JScrollPane logScrollPane = new JScrollPane(projectsToShareNames);
		        projectsToShareNames.append("Selected group : " + groupNamesList.getSelectedItem().toString() + "\n");
		        projectsToShareNames.append("Selected Access Level : " + accesLevelsList.getSelectedItem().toString() + "\n");
		        projectsToShareNames.append("------------------------------------\n \n");
		        boolean isSelected = false;
		        for(int i = 0; i < myProjectsList.length; i++) {
		        	if (myProjectsList[i].checkBox.isSelected()){
		        		projectsToShareNames.append("-  "+myProjectsList[i].btn.getText()+"\n \n");
		        		projectsToShareIds.add(myProjectsList[i].id);
		        		isSelected = true;
		      		}
				}
	        	if (!isSelected){
	        		JOptionPane.showMessageDialog(null, "No projects selected", "Error", JOptionPane.ERROR_MESSAGE);
	        	}
	        	else {
					int confirm = JOptionPane.showConfirmDialog(null,
							logScrollPane, "Do you want to share these projects ?", JOptionPane.YES_NO_OPTION);
			
					if (confirm == 0) {
						try {
							myProjects.shareProjects(projectsToShareIds,selectedGroupId, selectedAccessLevel);
							JOptionPane.showMessageDialog(null, 
							         "Projects added to group", 
							         "Sharing completed", 
							         JOptionPane.INFORMATION_MESSAGE, 
							         null);
							myProjects.update();
							displayProjects();
						} catch (GitLabApiException e1) {
			        		JOptionPane.showMessageDialog(null, "Error sharing projects to group : " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
	        	}
			}
		}
        
		else if (src == addGroupstoModuleMenuItem) {
  
			MyModules myModules = new MyModules();
			String[] groupNames = myModules.getModulesNames();
			JComboBox<String>  moduleNamesList = new JComboBox<String>(groupNames) ;

			Object[] message = {
			    "Module:", moduleNamesList,
			};

			int option = JOptionPane.showConfirmDialog(null, message, "Choose the Group and Access Level", JOptionPane.OK_CANCEL_OPTION);
			if (option == JOptionPane.OK_OPTION) {	
				String selectedModuleName = moduleNamesList.getSelectedItem().toString();
				
		       	ArrayList<Integer> groupIdsToAdd = new ArrayList<Integer>();
		       	JTextArea groupsToAddNames = new JTextArea(15,50);
		       	groupsToAddNames.setMargin(new Insets(5,5,5,5));
		       	groupsToAddNames.setEditable(false);
		        JScrollPane logScrollPane = new JScrollPane(groupsToAddNames);
		        groupsToAddNames.append("Selected module : " + moduleNamesList.getSelectedItem().toString() + "\n");
		        groupsToAddNames.append("------------------------------------\n \n");
		        boolean isSelected = false;
		        for(int i = 0; i < myGroupsList.length; i++) {
		        	if (myGroupsList[i].checkBox.isSelected()){
		        		groupsToAddNames.append("-  "+myGroupsList[i].btn.getText()+"\n \n");
		        		groupIdsToAdd.add(myGroupsList[i].id);
		        		isSelected = true;
		      		}
				}
	        	if (!isSelected){
	        		JOptionPane.showMessageDialog(null, "No projects selected", "Error", JOptionPane.ERROR_MESSAGE);
	        	}
	        	else {
					int confirm = JOptionPane.showConfirmDialog(null,
							logScrollPane, "Do you want to add these groups ?", JOptionPane.YES_NO_OPTION);
			
					if (confirm == 0) {
						Module module = myModules.getModule(selectedModuleName);
						myModules.addGroupsToModule(groupIdsToAdd,module,myGroups);
						JOptionPane.showMessageDialog(null, 
						         "Groups added to module", 
						         "Adding groups to module completed", 
						         JOptionPane.INFORMATION_MESSAGE, 
						         null);
					}
	        	}
			}
		}
        
        else if (src == deleteProjectsMenuItem) {
        	boolean isSelected = false;
	       	ArrayList<Integer> projectsToDeleteIds = new ArrayList<Integer>();
	       	JTextArea projectsToDeleteNames = new JTextArea(15,50);
	       	projectsToDeleteNames.setMargin(new Insets(5,5,5,5));
	       	projectsToDeleteNames.setEditable(false);
	        JScrollPane logScrollPane = new JScrollPane(projectsToDeleteNames);
	        for(int i = 0; i < myProjectsList.length; i++) {
	        	if (myProjectsList[i].checkBox.isSelected()){
	        		projectsToDeleteNames.append("-  "+myProjectsList[i].btn.getText()+"\n \n");
	        		projectsToDeleteIds.add(myProjectsList[i].id);
	        		isSelected = true;
	      		}
			}
        	if (!isSelected){
        		JOptionPane.showMessageDialog(null, "No projects selected", "Error", JOptionPane.ERROR_MESSAGE);
        	}
        	else {
				int confirm = JOptionPane.showConfirmDialog(null,
						logScrollPane, "Do you want to delete these projects ?", JOptionPane.YES_NO_OPTION);
		
				if (confirm == 0) {
					try {
						myProjects.deleteProjects(projectsToDeleteIds);
						JOptionPane.showMessageDialog(null, 
						         "Projects Deleted", 
						         "Deletion Completed", 
						         JOptionPane.INFORMATION_MESSAGE, 
						         null);
						myProjects.update();
						displayProjects();
					} catch (GitLabApiException e1) {
		        		JOptionPane.showMessageDialog(null, "Error while deleting projects : "+e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
        	}
        }
        
        else if (src == deleteGroupsMenuItem) {
            boolean isSelected = false;
	       	ArrayList<Integer> groupsToDeleteIds = new ArrayList<Integer>();
	       	JTextArea groupsToDeleteNames = new JTextArea(15,50);
	       	groupsToDeleteNames.setMargin(new Insets(5,5,5,5));
	       	groupsToDeleteNames.setEditable(false);
	        JScrollPane logScrollPane = new JScrollPane(groupsToDeleteNames);
	        for(int i = 0; i < myGroupsList.length; i++) {
	        	if (myGroupsList[i].checkBox.isSelected()){
	        		groupsToDeleteNames.append("-  "+myGroupsList[i].btn.getText()+"\n \n");
	        		groupsToDeleteIds.add(myGroupsList[i].id);
	        		isSelected = true;
	      		}
			}
        	if (!isSelected){
        		JOptionPane.showMessageDialog(null, "No groups selected", "Error", JOptionPane.ERROR_MESSAGE);
        	}
        	else {
				int confirm = JOptionPane.showConfirmDialog(null,
						logScrollPane, "Do you want to delete these groups ?", JOptionPane.YES_NO_OPTION);
		
				if (confirm == 0) {
					try {
						for (Integer id : groupsToDeleteIds) {
							ConnexionModel.getGitLabApi().getGroupApi().deleteGroup(id);
						}
						JOptionPane.showMessageDialog(null, 
						         "Groups Deleted", 
						         "Deletion Completed", 
						         JOptionPane.INFORMATION_MESSAGE, 
						         null);
			        } catch (GitLabApiException e3) {
		        		JOptionPane.showMessageDialog(null, "Error while deleting groups : " + e3.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			        }

				}
        	}
			myGroups.update();
        	displayGroups();
        }
        
        else if (src == deleteModulesMenuItem) {
            boolean isSelected = false;
	       	ArrayList<Integer> modulesToDeleteIndex = new ArrayList<Integer>();
	       	JTextArea modeulesToDeleteNames = new JTextArea(15,50);
	       	modeulesToDeleteNames.setMargin(new Insets(5,5,5,5));
	       	modeulesToDeleteNames.setEditable(false);
	        JScrollPane logScrollPane = new JScrollPane(modeulesToDeleteNames);
	        for(int i = 0; i < myModulesList.length; i++) {
	        	if (myModulesList[i].checkBox.isSelected()){
	        		modeulesToDeleteNames.append("-  "+myModulesList[i].btn.getText()+"\n \n");
	        		modulesToDeleteIndex.add(i);
	        		isSelected = true;
	      		}
			}
        	if (!isSelected){
        		JOptionPane.showMessageDialog(null, "No modules selected", "Error", JOptionPane.ERROR_MESSAGE);
        	}
        	else {
				int confirm = JOptionPane.showConfirmDialog(null,
						logScrollPane, "Do you want to delete these modules ?", JOptionPane.YES_NO_OPTION);
		
				if (confirm == 0) {
					myModules.deleteModules(modulesToDeleteIndex);
					JOptionPane.showMessageDialog(null, 
					         "Modules Deleted", 
					         "Deletion Completed", 
					         JOptionPane.INFORMATION_MESSAGE, 
					         null);
					myGroups.update();
		        	displayModules();

				}
        	}
        }
        
 
        
        
        else if (src == archiveMenu) {
        	boolean isSelected = false;
        	for (int i = 0; i < myProjectsList.length ; i++) {
        		if (myProjectsList[i].checkBox.isSelected()) {
        			try {
						dashboardController.archiveProject(myProjectsList[i].id);
					} catch (GitGudExceptions e1) {
		        		JOptionPane.showMessageDialog(null, "Error Archiving Project "+myProjectsList[i].btn.getText()+" : " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
        			isSelected = true;
        		}
        		
        		myProjectsList[i].checkBox.setSelected(false);
        	}
        	
        	//Si aucun project n'est séléctionné => message d'erreur
        	if (!isSelected){
        		JOptionPane.showMessageDialog(null, "No projects selected", "Error", JOptionPane.ERROR_MESSAGE);
        	}
        	else {
				JOptionPane.showMessageDialog(null, 
				         "Projects Archived", 
				         "Archiving Completed", 
				         JOptionPane.INFORMATION_MESSAGE, 
				         null);
				myProjects.update();
				displayProjects();
        	}
        }
        else if (src == unarchiveMenu) {
        	boolean isSelected = false;
        	for (int i = 0; i < myProjectsList.length ; i++) {
        		if (myProjectsList[i].checkBox.isSelected()) {
        			try {
						dashboardController.unarchiveProject(myProjectsList[i].id);
					} catch (GitGudExceptions e1) {
		        		JOptionPane.showMessageDialog(null, "Error Unrchiving Project "+myProjectsList[i].btn.getText()+" : " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					}
        			isSelected = true;
        		}
        		
        		myProjectsList[i].checkBox.setSelected(false);
        	}
        	//Si aucun project n'est séléctionné => message d'erreur
        	if (!isSelected){
        		JOptionPane.showMessageDialog(null, "No projects selected", "Error", JOptionPane.ERROR_MESSAGE);
        	}
        	else {
				JOptionPane.showMessageDialog(null, 
				         "Projects Unarchived", 
				         "Unarchiving Completed", 
				         JOptionPane.INFORMATION_MESSAGE, 
				         null);
				myProjects.update();
				displayProjects();
        	}
        }
        
        //Mode clair
        if (src == projectLightModeMenuItem) {
        	try {
        		UIManager.setLookAndFeel(light);
        	} catch( Exception ex ) {
        		System.err.println( "Failed to initialize LaF" );
        	}
      			SwingUtilities.updateComponentTreeUI(getRootPane());
        }
        //Mode Sombre
        else if (src == projectDarkModeMenuItem) {
        	try {
        		UIManager.setLookAndFeel(dark);
        	} catch( Exception ex ) {
        		System.err.println( "Failed to initialize LaF" );
        	}
        		SwingUtilities.updateComponentTreeUI(getRootPane());
        }
        //Mode clair
        else if (src == groupLightModeMenuItem) {
        	try {
        		UIManager.setLookAndFeel(light);
        	} catch( Exception ex ) {
        		System.err.println( "Failed to initialize LaF" );
        	}
      			SwingUtilities.updateComponentTreeUI(getRootPane());
        }
        //Mode Sombre
        else if (src == groupDarkModeMenuItem) {
        	try {
        		UIManager.setLookAndFeel(dark);
        	} catch( Exception ex ) {
        		System.err.println( "Failed to initialize LaF" );
        	}
      			SwingUtilities.updateComponentTreeUI(getRootPane());
        }
        //Mode clair
        else if (src == moduleLightModeMenuItem) {
        	try {
        		UIManager.setLookAndFeel(light);
        	} catch( Exception ex ) {
        		System.err.println( "Failed to initialize LaF" );
        	}
      			SwingUtilities.updateComponentTreeUI(getRootPane());
        }
        //Mode Sombre
        else if (src == moduleDarkModeMenuItem) {
        	try {
        		UIManager.setLookAndFeel(dark);
        	} catch( Exception ex ) {
        		System.err.println( "Failed to initialize LaF" );
        	}
        		SwingUtilities.updateComponentTreeUI(getRootPane());
        }
        
        else if (dashboardTabbedPane.getSelectedComponent().equals(myProjectsPanel)){
        	  for(int i = 0; i < myProjectsList.length; i++)
              {
          		if (src==myProjectsList[i].btn){
          			 // Checks if a project is clicked
          			try {
          				ProjectController pjController =  new ProjectController(myProjects);
  						List<String> branches = pjController.getBranches(myProjectsList[i].id);
  						if (branches.size() == 0) {
  							JOptionPane.showMessageDialog(null,"Project " + myProjectsList[i].btn.getText() + " is empty");
  						} else {
  							ProjectView np_view = null;
  							np_view = new ProjectView(myProjectsList[i].btn.getText(),myProjectsList[i].id , i , pjController);
  							np_view.addPropertyChangeListener(this);
  			             	np_view.setVisible(true);
  						}
  					} catch (GitGudExceptions e1) {
  						JOptionPane.showMessageDialog(null,"Error : " + e1.getMessage());
  					} 
          		}
          		
          		else if (src==myProjectsList[i].checkBox) {
          			if (myProjectsList[i].checkBox.isSelected()) {
          				if (lastSelectedProject != -1 && isShiftPressed && i != lastSelectedProject) {
          					int min = Math.min(i, lastSelectedProject);
          					int max = Math.max(i, lastSelectedProject);
          					for(int j = min+1; j <= max; j++) {
          						myProjectsList[j].checkBox.setSelected(true);
          					}
          				}
          				lastSelectedProject = i;
          				lastUnselectedProject = -1;
          			}
          			else {
          				if (lastUnselectedProject != -1 && isShiftPressed && i != lastUnselectedProject) {
          					int min = Math.min(i, lastUnselectedProject);
          					int max = Math.max(i, lastUnselectedProject);
          					for(int j = min+1; j <= max; j++) {
          						myProjectsList[j].checkBox.setSelected(false);
          					}
          				}
          				lastUnselectedProject = i;
          				lastSelectedProject = -1;

          			}
          		}
              }
          }
        
          else if (dashboardTabbedPane.getSelectedComponent().equals(myGroupsPanel)){
          	//Group Navigation
          	if (src==previousBtn) {
      			if (parentGroupId != null) {
      				displayGroups();
      				parentGroupId = myGroups.getGroup(parentGroupId).getParentId();
      			}
      			else {
      				displayGroups();
      			}
          	}
          	else {
  	        	for(int i = 0; i < myGroupsList.length; i++){
  	        		if (src==myGroupsList[i].btn){
  	        			Integer newparentGroupId = myGroups.getGroup(myGroupsList[i].id).getParentId();
  	        			parentGroupId = myGroupsList[i].id;
  	        			displayGroups();
  	        			parentGroupId = newparentGroupId;
  	        			
  	        		}
  	        		
  	        		else if (src==myGroupsList[i].checkBox) {
  	          			// Multi-selection
  	        			if (myGroupsList[i].checkBox.isSelected()) {
  	        				if (lastSelectedGroup != -1 && isShiftPressed && i != lastSelectedGroup) {
  	        					int min = Math.min(i, lastSelectedGroup);
  	        					int max = Math.max(i, lastSelectedGroup);
  	        					for(int j = min+1; j <= max; j++) {
  	        						myGroupsList[j].checkBox.setSelected(true);
  	        					}
  	        				}
  	        				lastSelectedGroup = i;
  	        				lastUnselectedGroup = -1;
  	        			}
  	        			else {
  	        				if (lastUnselectedGroup != -1 && isShiftPressed && i != lastUnselectedGroup) {
  	        					int min = Math.min(i, lastUnselectedGroup);
  	        					int max = Math.max(i, lastUnselectedGroup);
  	        					for(int j = min+1; j <= max; j++) {
  	        						myGroupsList[j].checkBox.setSelected(false);
  	        					}
  	        				}
  	        				lastUnselectedGroup = i;
  	        				lastSelectedGroup = -1;

  	        			}
  	        		}
  	        		
  	        	}
          	}
         }
          
          
          else if (dashboardTabbedPane.getSelectedComponent().equals(myModulesPanel)){
          	  for(int i = 0; i < myModulesList.length; i++)
                {
            		if (src==myModulesList[i].btn){
            			 // Checks if a module is clicked
   
            		}
            		
            		else if (src==myModulesList[i].checkBox) {
            			// Multi-selection
            			if (myModulesList[i].checkBox.isSelected()) {
            				if (lastSelectedModule != -1 && isShiftPressed && i != lastSelectedModule) {
            					int min = Math.min(i, lastSelectedModule);
            					int max = Math.max(i, lastSelectedModule);
            					for(int j = min+1; j <= max; j++) {
            						myModulesList[j].checkBox.setSelected(true);
            					}
            				}
            				lastSelectedModule = i;
            				lastUnselectedModule = -1;
            			}
            			else {
            				if (lastUnselectedModule != -1 && isShiftPressed && i != lastUnselectedModule) {
            					int min = Math.min(i, lastUnselectedModule);
            					int max = Math.max(i, lastUnselectedModule);
            					for(int j = min+1; j <= max; j++) {
            						myModulesList[j].checkBox.setSelected(false);
            					}
            				}
            				lastUnselectedModule = i;
            				lastSelectedModule = -1;

            			}
            		}
                }
            }
        
    }

	
	/** 
	 * @param e
	 */
	@Override
	public void stateChanged(ChangeEvent e) {
		//We reset the multiple selection variable
		lastSelectedProject = -1;
		lastUnselectedProject = -1;
		lastSelectedGroup = -1;
		lastUnselectedGroup = -1;
		lastSelectedModule = -1;
	    lastUnselectedModule = -1;
		JPanel c = (JPanel) dashboardTabbedPane.getSelectedComponent();
		if (c.equals(myProjectsPanel)) { 
			myProjects.update();
			displayProjects();
		}
		
		else if (c.equals(myGroupsPanel)) { 
			myGroups.update();
			displayGroups();
		}
		
		else if (c.equals(myModulesPanel)) { 
			myModules.update();
			displayModules();
		}

		c.revalidate();
		c.repaint();
	}
	
    
	@Override
	public void keyPressed(KeyEvent e) {
		
		if (e.getKeyCode() == 16 &&  !isShiftPressed) {
			isShiftPressed = true;
		}
	}


	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == 16) {
			isShiftPressed = false;
		}
	}


	@Override
	public void keyTyped(KeyEvent e) {		
	}

}


