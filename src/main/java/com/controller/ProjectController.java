package com.controller;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.Member;

import com.model.*;


public class ProjectController {
    private MyProjects pj_model ;
    private Project pj_model_local;
    private GitLabApi gitLabApi;
    
    public ProjectController(MyProjects myProjects) {
    	pj_model_local = new Project();
    	this.pj_model = myProjects;
    }
    
    
    /** 
     * @param csv_file
     * @throws GitGudExceptions 
     */
    public void addProjectsFromCsv(File csv_file) throws GitGudExceptions {
    	pj_model.addProjectsFromCsv(csv_file);
    	updateProject();
    }
    
    
    /** 
     * @param id
     * @return List<String>
     * @throws GitGudExceptions
     */
    public List<String> getBranches(int id) throws GitGudExceptions {
    	try {
    		return pj_model_local.getBranches(id);
    	}catch (Exception e) {
			throw new GitGudExceptions(e.getMessage());
		}
    }
    
    
    /** 
     * @param id
     * @return String
     * @throws GitGudExceptions
     */
    public String getNameProject(int id) throws GitGudExceptions {
    	try {
    		return pj_model.getProjects().get(id).getNameWithNamespace();
    	}catch (Exception e) {
			throw new GitGudExceptions(e.getMessage());
		}
    }
    
    
    /** 
     * @param idOrPath
     * @param branche
     * @return List<String>
     * @throws GitGudExceptions
     */
    public List<String> getFolder(Object idOrPath, String branche) throws GitGudExceptions{
    	try {
    		return pj_model_local.getFolder(idOrPath, branche);
    	} catch (Exception e) {
    		throw new GitGudExceptions("ERROR : Unable to upload folders");
    	}
    	
    }
    
    
    /** 
     * @param idOrPath
     * @param path
     * @param branche
     * @return List<String>
     * @throws GitGudExceptions
     */
    //Surchage de la méthode getFolder
    public List<String> getFolder(Object idOrPath, String path, String branche) throws GitGudExceptions{
    	try {
    		return pj_model_local.getFolder(idOrPath, path, branche);
    	}catch (Exception e) {
    		throw new GitGudExceptions("ERROR : Unable to upload folders");
    	}
    }
    
    
    /** 
     * @param idOrPath
     * @param branche
     * @return List<String>
     * @throws GitGudExceptions
     */
    public List<String> getFile(Object idOrPath, String branche) throws GitGudExceptions{
    	try {
    		return pj_model_local.getFile(idOrPath, branche);
    	}catch (Exception e) {
    		throw new GitGudExceptions("ERROR : Unable to upload files");
    	}
    	
    }
    
    
    /** 
     * @param idOrPath
     * @param path
     * @param branche
     * @return List<String>
     * @throws GitGudExceptions
     */
    //Surcharge de la méthode getFile
    public List<String> getFile(Object idOrPath, String path, String branche) throws GitGudExceptions{
    	try {
    		return pj_model_local.getFile(idOrPath, path, branche);
    	}catch (Exception e) {
    		throw new GitGudExceptions("ERROR : Unable to upload files");
    	}
    }
    
    
    /** 
     * @param idOrPath
     * @return List<Member>
     * @throws GitGudExceptions
     */
    public List<Member> getUsers(int idOrPath) throws GitGudExceptions{
    	try {
    		return pj_model_local.getUsers(idOrPath);
    	}catch (Exception e) {
    		throw new GitGudExceptions("ERROR : Unable to upload members of the projects");
    	}
    }
    
    
    /** 
     * @param id
     * @return Integer
     * @throws GitGudExceptions
     */
    public Integer getOpenIssuesCount(int id) throws GitGudExceptions {
    	try {
    		return pj_model.getProjects().get(id).getOpenIssuesCount();
    	}catch (Exception e) {
    		throw new GitGudExceptions("ERROR : Unable to upload the open issues of the projects");
    	}
    }
    
    /** 
     * @throws GitGudExceptions
     */
    public void updateProject() throws GitGudExceptions {
    	try {
        	pj_model.update();
    	}catch (Exception e) {
    		throw new GitGudExceptions("ERROR : Unable to update data");
    	}
    }
    
    
    /** 
     * @param id
     * @return String
     * @throws GitGudExceptions
     */
    public String getWebUrl(int id) throws GitGudExceptions {
    	try {
    		return pj_model.getProjects().get(id).getWebUrl();
    	} catch (Exception e) {
    		throw new GitGudExceptions("ERROR : Unable to uploard web url of the project");
    	}
    }
    
    
    /** 
     * @param id
     * @return String
     * @throws GitGudExceptions
     */
    public String getVisibility(int id) throws GitGudExceptions {
    	try {
    		return pj_model.getProjects().get(id).getVisibility().toString();
    	}catch (Exception e) {
    		throw new GitGudExceptions("ERROR : Unable to upload the visibility of the project");
    	}
    }
   
    
    /** 
     * @param idOrPath
     * @param branche
     * @return int
     * @throws GitGudExceptions
     */
    public int getNumberOfCommit(int idOrPath, String branche) throws GitGudExceptions {
    	try {
    		return pj_model_local.getNumberCommits(idOrPath, branche);
    	}catch (Exception e) {
    		throw new GitGudExceptions("ERROR : Unable to upload commits of the project");
    	}

    }
    
    
    /** 
     * @param idOrPath
     * @param branche
     * @return Commit
     * @throws GitGudExceptions
     */
    public Commit getLastCommit(int idOrPath, String branche) throws GitGudExceptions {
    	try {
    		return pj_model_local.getLastCommit(idOrPath, branche);
    	}catch (Exception e) {
    		throw new GitGudExceptions("ERROR : Unable to upload the last commit of the branch");
    	}
    }

    /** 
     * @param idOrPath
     * @param branche
     * @return Map<String, Float>
     * @throws GitGudExceptions
     */
    public Map<String, Float> getProjectLanguages(int idOrPath)  throws GitGudExceptions {
    	try {
    		return pj_model_local.getProjectLanguages(idOrPath, gitLabApi);
    	}catch (Exception e) {
    		throw new GitGudExceptions("ERROR : Unable to upload the languages used in the project");
    	}
    }
}
