package com.controller;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.Constants.TokenType;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.ProjectFilter;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.api.controler.UserAPI;
import com.model.AES;
import com.model.ConnexionModel;
import com.model.UserGit;

public class ConnexionController {

	/** 
	 * Returns true if the database JSON file is found or tries to create it
	 * @param path Path to database JSON file
	 * @return Boolean
	 * @throws GitGudExceptions
	 */
	// Vérifier si la BD existe
	public static Boolean checkDB(String path) throws GitGudExceptions {
		File file = new File(path);
		if(file.exists()) {
			return true;
		}
		else {
			// Essaye de la créer
			try(FileWriter fileWriter = new FileWriter(ConnexionModel.getDbconnectPath())) {
				fileWriter.write("{}");
				return true;
			} catch (Exception e1) {
				throw new GitGudExceptions("ERROR : Unable to create database. Check permissions.");
			}
		}
	}


	/** 
	 * Return data of the parsed JSON label
	 * @param path Path to database JSON file
	 * @param obj Label to find data in JSON
	 * @return Object Data linked with the specified label
	 * @throws GitGudExceptions
	 */
	// Accéder à la BDD
	public static Object getJSONObjet(String path, String obj) throws GitGudExceptions {
		// Accède à la base de donnée
		JSONParser parser = new JSONParser();
		try (Reader reader = new FileReader(path)) {
			JSONObject jsonObject;
			jsonObject = (JSONObject)parser.parse(reader);
			return jsonObject.get(obj);
		} catch (IOException e) {
			throw new GitGudExceptions("ERROR : Unable to create database. Check permissions.");
		}catch (ParseException e) {
			throw new GitGudExceptions("ERROR : Unable to read the database. Check the integrity of the file.");
		}
	}



	/** 
	 * Load and return the default Gitlab instance URL 
	 * @return String
	 */
	public static String getGitlabDefaultURL() {
		// Si la base de donnée existe
		try {
			if(checkDB(ConnexionModel.getConfigPath())){
				// Accède à la base de donnée
				Object defaultGitlabURL = getJSONObjet(ConnexionModel.getConfigPath(), "defaultGitlabURL");
				if(defaultGitlabURL != null) {
					ConnexionModel.setGitlab_default_URL(defaultGitlabURL.toString());
					return defaultGitlabURL.toString();	
				}
			}
		} catch (GitGudExceptions e) {
			return "";
		}
		return "";
	}


	/** 
	 * Set the default Gitlab instance URL 
	 * @param newURL
	 * @throws GitGudExceptions
	 */
	public static void setGitlabDefaultURL(String newURL) throws GitGudExceptions {
		// Si la base de donnée existe
		if(checkDB(ConnexionModel.getConfigPath())){
			// Accède à la base de donnée
			JSONParser parser = new JSONParser();
			try (Reader reader = new FileReader(ConnexionModel.getConfigPath())) {
				JSONObject defaultGitlabURL = (JSONObject) parser.parse(reader);           			    
				defaultGitlabURL.put("defaultGitlabURL", newURL);
				try {
					Files.write(Paths.get(ConnexionModel.getConfigPath()), defaultGitlabURL.toString().getBytes());
					ConnexionModel.setGitlab_default_URL(newURL);
				}catch (IOException e1) {
					throw new GitGudExceptions("WARNING : Unable to save the new Gitlab URL. Check permissions.");
				}
			}catch(IOException | ParseException e1) {
				throw new GitGudExceptions("WARNING : Unable to save the new Gitlab URL. Check file and permissions.");
			}
		}
	}


	/** 
	 * Return the last recorded Username and Gitlab instance URL for a Gitlab type connection 
	 * @return String[]
	 */
	public static String[] getLastGitlabConnection() {
		String[] connexionInfos = new String[] {"", ""};
		// Si la base de donnée existe
		try {
			if(checkDB(ConnexionModel.getConfigPath())){
				// Accède à la base de donnée
				Object lastGitlabConnexion = getJSONObjet(ConnexionModel.getConfigPath(), "lastGitlabConnexion");
				if(lastGitlabConnexion != null) {
					JSONObject userInfos = (JSONObject)lastGitlabConnexion;
					connexionInfos[0] = userInfos.get("username").toString();
					connexionInfos[1] = userInfos.get("url").toString();
					return connexionInfos;
				}	
			}
		} catch (GitGudExceptions e) {
			return connexionInfos;
		}
		return connexionInfos;
	}


	/** 
	 * Return the last recorded Username and Password for a Local type connection
	 * @return String[]
	 */
	public static String[] getLastLocalConnection() {
		String[] connexionInfos = new String[] {"", ""};
		// Si la base de donnée existe
		try {
			if(checkDB(ConnexionModel.getConfigPath())){
				// Accède à la base de donnée
				Object lastLocalConnexion = getJSONObjet(ConnexionModel.getConfigPath(), "lastLocalConnexion");
				if(lastLocalConnexion != null) {
					JSONObject userInfos = (JSONObject)lastLocalConnexion;
					connexionInfos[0] = userInfos.get("username").toString();
					connexionInfos[1] = AES.decrypt(userInfos.get("password").toString(), ConnexionModel.getSecretEncryptKey());
					return connexionInfos;
				}	
			}
		} catch (GitGudExceptions e) {
			return connexionInfos;
		}
		return connexionInfos;
	}


	/** 
	 *  Save Username and Gitlab instance URL for a Gitlab type connection 
	 * @param username
	 * @param url
	 * @throws GitGudExceptions
	 */
	public static void saveLastGitConnection(String username, String url) throws GitGudExceptions {
		// Si la base de donnée existe
		if(checkDB(ConnexionModel.getConfigPath())){
			// Accède à la base de donnée
			JSONParser parser = new JSONParser();
			try (Reader reader = new FileReader(ConnexionModel.getConfigPath())) {
				JSONObject lastGitlabConnexion = (JSONObject) parser.parse(reader);
				// Création de            			    
				JSONObject connexionInfos = new JSONObject();
				connexionInfos.put("username", username);
				connexionInfos.put("url", url);
				lastGitlabConnexion.put("lastGitlabConnexion", connexionInfos);

				try {
					Files.write(Paths.get(ConnexionModel.getConfigPath()), lastGitlabConnexion.toString().getBytes());
				}catch (IOException e1) {
					throw new GitGudExceptions("WARNING : Unable to save remember me informations. Check permissions.");
				}
			}catch(IOException | ParseException e1) {
				throw new GitGudExceptions("WARNING : Unable to save remember me informations. Check file and permissions.");
			}
		}
	}


	/** 
	 * Save Username and Password for a Local type connection
	 * @param username
	 * @param password
	 * @throws GitGudExceptions
	 */
	public static void saveLastLocalConnection(String username, String password) throws GitGudExceptions {
		// Si la base de donnée existe
		if(checkDB(ConnexionModel.getConfigPath())){
			// Accède à la base de donnée
			JSONParser parser = new JSONParser();
			try (Reader reader = new FileReader(ConnexionModel.getConfigPath())) {
				JSONObject lastLocalConnexion = (JSONObject) parser.parse(reader);
				JSONObject connexionInfos = new JSONObject();
				connexionInfos.put("username", username);
				connexionInfos.put("password", AES.encrypt(password, ConnexionModel.getSecretEncryptKey()));
				lastLocalConnexion.put("lastLocalConnexion", connexionInfos);
				try {
					Files.write(Paths.get(ConnexionModel.getConfigPath()), lastLocalConnexion.toString().getBytes());
				}catch (IOException e1) {
					throw new GitGudExceptions("WARNING : Unable to save remember me informations. Check permissions.");
				}

			}catch(IOException | ParseException e1) {
				throw new GitGudExceptions("WARNING : Unable to save remember me informations. Check file and permissions.");
			}
		}
	}

	/** 
	 * Save user information in JSON database
	 * @param usr New saved username
	 * @param psw New saved password
	 * @param pswConf Confirmation of new saved password
	 * @param url New saved Gitlab instance URL
	 * @param token New saved token connection
	 * @return Boolean Return true if record went well
	 * @throws GitGudExceptions
	 */
	public static Boolean editProfilSettings(String usr, String psw, String pswConf, String url, String token) throws GitGudExceptions {
		// Si tous les champs sont remplis
		if(!(usr.equals("") || psw.equals("") || pswConf.equals("") || url.equals("") || token.equals(""))){
			if(psw.equals(pswConf)) {
				try {
					URL urlGitlab = new URL(url);
				} catch (MalformedURLException e) {
					throw new GitGudExceptions("Enable to connect to Gitlab instance check URL.");
				}
				if (ConnexionController.testTokenDistantConnection(url, token, TokenType.PRIVATE)){
					// Si la base de donnée existe
					if(checkDB(ConnexionModel.getDbconnectPath())){
						// Accède à la base de donnée
						Object newUser = getJSONObjet(ConnexionModel.getDbconnectPath(), usr);
						if(newUser == null) {
							JSONParser parser = new JSONParser();
							try (Reader reader = new FileReader(ConnexionModel.getDbconnectPath())) {
								JSONObject user = (JSONObject) parser.parse(reader);
								JSONObject userInfos = new JSONObject();

								userInfos.put("url", url);
								userInfos.put("password", AES.encrypt(psw, ConnexionModel.getSecretEncryptKey()));
								userInfos.put("token", AES.encrypt(token, ConnexionModel.getSecretEncryptKey()));
								user.remove(ConnexionModel.getUsername());
								user.put(usr, userInfos);

								try {
									Files.write(Paths.get(ConnexionModel.getDbconnectPath()), user.toString().getBytes());
									return true;
								}catch (IOException e1) {
									throw new GitGudExceptions("WARNING : Unable to save new setings informations. Check permissions.");
								}
							}catch(IOException | ParseException e1) {
								throw new GitGudExceptions("WARNING : Unable to save save new setings informations. Check file and permissions.");
							}
						}
						else {
							throw new GitGudExceptions("Username already exist.");
						}
					}
					else {
						throw new GitGudExceptions("Enable to connect to Gitlab instance check URL or token.");
					}
				}
				else {
					throw new GitGudExceptions("Unable to access the database. Check access rights.");
				}
			}
			else {
				throw new GitGudExceptions("Passwords do not match.");
			}
		}
		else {
			throw new GitGudExceptions("All fields are required.");
		}
	}

	/** 
	 * Return all user informations contains in JSON database
	 * @return Map<String, String>
	 * @throws GitGudExceptions
	 */
	public static Map<String, String> getProfilSettings() throws GitGudExceptions {
		Map<String, String> profilSettings = new HashMap<String, String>();
		// Si la base de donnée existe
		try {
			if(checkDB(ConnexionModel.getDbconnectPath())){
				// Accède à la base de donnée
				Object user = getJSONObjet(ConnexionModel.getDbconnectPath(), ConnexionModel.getUsername());
				if(user != null) {
					JSONObject userInfos = (JSONObject)user;
					profilSettings.put("password", AES.decrypt(userInfos.get("password").toString(), ConnexionModel.getSecretEncryptKey()));
					profilSettings.put("url", userInfos.get("url").toString());
					profilSettings.put("token", AES.decrypt(userInfos.get("token").toString(), ConnexionModel.getSecretEncryptKey()));
					return profilSettings;
				}else {
					throw new GitGudExceptions("Unable to find user");
				}
			}else {
				throw new GitGudExceptions("Unable to acces to data base");
			}
		}
		catch(Exception e){
			throw new GitGudExceptions("ERROR : " + e.getMessage());
		}
	}


	/** 
	 * Try to connect with a Local type connection
	 * @param username
	 * @param password
	 * @return Boolean Return true if connection is successful
	 * @throws GitGudExceptions
	 */
	public static Boolean localConnection(String username, String password) throws GitGudExceptions {
		// Vérifier si les champs ne sont pas vides
		if(! (username.equals("") || password.equals(""))) {	    
			// Si la base de donnée existe
			if(checkDB(ConnexionModel.getDbconnectPath())){
				// Accède à la base de donnée
				JSONObject userDB = (JSONObject)getJSONObjet(ConnexionModel.getDbconnectPath(), username);
				// Si l'utilisateur existe
				if(userDB != null) {
					// Si le mot de passe correspond au mot de passe enregistré
					if(compareMdp(password, (String)userDB.get("password"))) {
						String url = (String)userDB.get("url");
						String token = AES.decrypt((String)userDB.get("token"), ConnexionModel.getSecretEncryptKey());
						// Si la connexion est possible
						if(ConnexionController.testTokenDistantConnection(url, token, TokenType.PRIVATE)) {
							// Création d'un nouvel USER API
							UserGit userAPIGit = new UserGit(token);
							ConnexionModel.setUserGitApi(userAPIGit);
							// Création git4J
							try {
								ConnexionModel.setUserApi(new UserAPI(new URL(url)));
							} catch (MalformedURLException e) {
								throw new GitGudExceptions(e.getMessage());
							}
							ConnexionModel.setGitLabApi(new GitLabApi(url, token));
							ConnexionModel.setUsername(username);
							ConnexionModel.setIsALocalUser(true);
							return true;
						}
						else {
							throw new GitGudExceptions("Enable to connect to Gitlab.");	
						}
					}
					else {
						throw new GitGudExceptions("Wrong password.");
					}
				}else {
					throw new GitGudExceptions("This username does not exist.");
				}
			}
			else {
				throw new GitGudExceptions("ERROR : Unable to access database. Check file and permissions.");
			}
		} else {
			throw new GitGudExceptions("ERROR : Empty field : all fields are required.");		
		}
	}



	/** 
	 * Try to connect with a Local type connection
	 * @param usr
	 * @param psw
	 * @param url
	 * @return Boolean Return true if connection is successful
	 * @throws GitGudExceptions
	 */
	public static Boolean gitlabConnexion(String usr, String psw, String url) throws GitGudExceptions {
		// Si les champs ne sont pas vides
		if((!(usr.equals("") || psw.equals("") || url.equals("")))) {				
			try {
				// Création d'un nouvel USER API
				UserAPI userAPI = new UserAPI(new URL(url));
				UserGit userAPIGit =  new UserGit(userAPI, usr, psw);
				ConnexionModel.setUserGitApi(userAPIGit);
				String token = ConnexionModel.getUserGitApi().getToken();

				// Si la connexion est possible
				if(ConnexionController.testTokenDistantConnection(url, token, TokenType.OAUTH2_ACCESS)) {
					// Création git4J
					ConnexionModel.setUserApi(userAPI);
					ConnexionModel.setGitLabApi(new GitLabApi(url, TokenType.OAUTH2_ACCESS, token));
					ConnexionModel.setUsername(usr);
					ConnexionModel.setIsALocalUser(false);
					return true;
				}
				else {
					throw new GitGudExceptions("Enable to connect to Gitlab.");	
				}

			} catch (MalformedURLException e) {
				throw new GitGudExceptions("Unable to connect: " + e.getMessage());	
			}
			catch (GitLabApiException e) {
				throw new GitGudExceptions(e.getMessage());
			}
		}else {
			throw new GitGudExceptions("ERROR : Empty field : all fields are required.");
		}
	}




	/** 
	 * Checks the match of two passwords
	 * @param clearMdp 
	 * @param encryptMdp
	 * @return Boolean Return true if password are equals
	 */
	public static Boolean compareMdp(String clearMdp, String encryptMdp) {
		return clearMdp.equals(AES.decrypt(encryptMdp, ConnexionModel.getSecretEncryptKey()));
	}


	/** 
	 * Try to execute Gitlab API request to check the distant connection
	 * @param URL Gitlab instance URL
	 * @param token Gitlab API token
	 * @param type Gitlab API token type (PRIVATE/ OAUTH2_ACCESS)
	 * @return Boolean Return true if request is successful
	 * @throws GitGudExceptions
	 */
	public static Boolean testTokenDistantConnection(String URL, String token, Constants.TokenType type) throws GitGudExceptions {
		GitLabApi gitLabApi = new GitLabApi(URL, type, token);
		try {
			gitLabApi.getProjectApi().getProjects(new ProjectFilter().withMembership(true), 1);
		} catch (GitLabApiException e) {
			if(e.getMessage().contains("401")) {
				throw new GitGudExceptions("ERROR : GitLab connection not possible: check ids.");	
			}
			throw new GitGudExceptions("ERROR : Enable to connect to GitLab instance. " + e.getMessage());
		}catch (Exception e) {
			throw new GitGudExceptions("ERROR : Enable to connect to GitLab instance. " + e.getMessage());
		}
		return true;
	}

	public static Boolean createAccount(String usr, String psw, String pswConf, String url, String token) throws GitGudExceptions {
		if(!(usr.equals("") || psw.equals("") || pswConf.equals("") || url.equals("") || token.equals(""))){
			try {
				//Vérifier si la BD existe sinon la créer
				if(ConnexionController.checkDB(ConnexionModel.getDbconnectPath())) {
					JSONParser parser = new JSONParser();
					// Lecture de la BD
					try (Reader reader = new FileReader(ConnexionModel.getDbconnectPath())) {
						JSONObject jsonObject = (JSONObject) parser.parse(reader);
						// Si l'utilisateur n'exsite pas déjà
						if(jsonObject.get(usr) == null) {
							// Si les deux mot de passe sont égaux
							if(psw.equals(pswConf)) {
								URL urlGitlab = new URL(url);
								if (ConnexionController.testTokenDistantConnection(url, token, TokenType.PRIVATE)){
									// Création de l'user            			    
									JSONObject userInfos = new JSONObject();
									userInfos.put("url", url);
									userInfos.put("password", AES.encrypt(psw, ConnexionModel.getSecretEncryptKey()));
									userInfos.put("token", AES.encrypt(token, ConnexionModel.getSecretEncryptKey()));
									jsonObject.put(usr, userInfos);
									// Ecriture dans la BD
									Files.write(Paths.get(ConnexionModel.getDbconnectPath()), jsonObject.toString().getBytes());
									return true;
								}
								else {
									throw new GitGudExceptions("Wrong URL or token.");
								}
							}
							else {
								throw new GitGudExceptions("Passwords do not match.");
							}
						}
						else {
							throw new GitGudExceptions("Username " + usr + " already exists.");
						}
					} catch (IOException e1) {
						throw new GitGudExceptions("Unable to access the database. Check access rights.");
					}
				}else {
					throw new GitGudExceptions("Unable to access the database. Check access rights.");
				}
			} catch (HeadlessException | GitGudExceptions e1) {
				throw new GitGudExceptions(e1.getMessage());
			}
			catch(Exception e1) {
				throw new GitGudExceptions("ERROR : " + e1.getMessage());	
			}
		}
		else {
			throw new GitGudExceptions("All fields are required.");
		}
	}
}

