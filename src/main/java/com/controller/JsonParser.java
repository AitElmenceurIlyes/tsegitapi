package com.controller;
import java.io.IOException;
import java.nio.file.Paths;
import com.fasterxml.jackson.databind.ObjectMapper;
public class JsonParser {
    private static JsonParser jsonParser = null;
    private static ObjectMapper objectMapper;
    public JsonParser(){
        objectMapper= new ObjectMapper();
    }
 
 /** 
  * @return ObjectMapper
  */
 public ObjectMapper getObjectMapper(){
     return objectMapper;
 }
    
    /** 
     * @return JsonParser
     * return the instance of the singleton
     */
    public static JsonParser getInstance()
    {
        if (jsonParser == null){
                jsonParser = new JsonParser();
            }
 
        return jsonParser;
    }
    
    /** 
     * @param o
     * Object to write
     * @param path
     * path of the json file
     */
    public static void writeFile(Object o,String path){
        try {
            objectMapper.writeValue(Paths.get(path).toFile(), o);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    
    /** 
     * @param classtype
     *  Precise class type , for example if we want to build a Module object , write Module.class
     * @param path
     * path of the json file
     * @return Object[]
     * Return a list of object , need to be cast rightfully
     */
    public static Object readFile(Class classtype,String path){
        try {
            return objectMapper.readValue(Paths.get(path).toFile(),classtype);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
