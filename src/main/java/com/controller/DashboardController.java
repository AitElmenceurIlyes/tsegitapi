package com.controller;

import java.io.File;

import com.model.*;
import com.view.*;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;

public class DashboardController {
    private MyProjects pj_model;
    private DashboardView pj_view;

    public DashboardController(MyProjects m) {
        this.pj_model = m;
    }

    
    /** 
     * @param view
     */
    public void setView(DashboardView view) {
        this.pj_view = view;
    }

    
    /** 
     * @param csv_file
     */
    public void addProjectsFromCsv(File csv_file) throws GitGudExceptions {
        try {
            pj_model.addProjectsFromCsv(csv_file);
        
        updateProject();
    } catch (GitGudExceptions e) {
		throw new GitGudExceptions("Unable to add projects from CSV : "+e.getMessage());
	}
    }

    public void updateProject() {
        pj_model.update();
    }
    
    /** 
     * @return MyProjects
     */
    public MyProjects getProjects(){
        return pj_model;
    }
    
    /** 
     * @param archived
     * @throws GitGudExceptions
     */
    public void getProjects(boolean archived) throws GitGudExceptions {
        try {
            pj_model.getProjects(archived);
        } catch (GitLabApiException e) {
            throw new GitGudExceptions(e.getMessage());
        }
    }

    
    /** 
     * @param id
     * @throws GitGudExceptions
     */
    public void archiveProject(Integer id) throws GitGudExceptions {
        try {
            ConnexionModel.getGitLabApi().getProjectApi().archiveProject(id);
        } catch (GitLabApiException e) {
            throw new GitGudExceptions(e.getMessage());
        }
    }

    
    /** 
     * @param id
     * @throws GitGudExceptions
     */
    public void unarchiveProject(Integer id) throws GitGudExceptions {
        try {
            ConnexionModel.getGitLabApi().getProjectApi().unarchiveProject(id);
        } catch (GitLabApiException e) {
            throw new GitGudExceptions(e.getMessage());
        }
    }

}
