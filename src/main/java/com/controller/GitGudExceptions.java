package com.controller;

public class GitGudExceptions extends Exception {

	  public GitGudExceptions() {
	    super();
	  }

	  public GitGudExceptions(String s) {
	    super(s);
	  }
}
