package com.model;

import java.util.ArrayList;

import com.api.controler.UserAPI;

import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.User;

public class UserGit extends org.gitlab4j.api.models.User {
    private ArrayList<Project> projects;
    private String password;
    private String token;

    public UserGit(String token) {
        super();
        this.token = token;
        this.projects = new ArrayList<Project>();
    }

    public UserGit() {
        super();

    }

    public UserGit(int id, String username, ArrayList<Project> projectList) {
        setId(id);
        setEmail(username);
        projects = projectList;
    }

    public UserGit(int id, String username) {
        setId(id);
        setUsername(username);
        this.projects = new ArrayList<Project>();
    }

    public UserGit(UserAPI userAPI, String username, String password) throws GitLabApiException {
        this.password = password;
        this.token = getAuthToken(userAPI, username, password);
        try {
            UserGit(userAPI.getUserByUsername(username).get(0));
        } catch (GitLabApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    
    /** 
     * Token getter
     * @return String
     */
    public String getToken() {
        return token;
    }

    /**
     * Copy constructor
     * 
     * @param userGit
     */
    private void UserGit(UserGit userGit) {
        setAvatarUrl(userGit.getAvatarUrl());
        setBio(userGit.getBio());
        setBot(userGit.getBot());
        setCanCreateGroup(userGit.getCanCreateGroup());
        setCanCreateProject(userGit.getCanCreateProject());
        setColorSchemeId(userGit.getColorSchemeId());
        setConfirmedAt(userGit.getConfirmedAt());
        setCreatedAt(userGit.getConfirmedAt());
        setCurrentSignInAt(userGit.getCurrentSignInAt());
        setCustomAttributes(userGit.getCustomAttributes());
        setEmail(userGit.getEmail());
        setExternUid(userGit.getExternUid());
        setExternal(userGit.getExternal());
        setExtraSharedRunnersMinutesLimit(userGit.getExtraSharedRunnersMinutesLimit());
        setId(userGit.getId());
        setIdentities(userGit.getIdentities());
        setIsAdmin(userGit.getIsAdmin());
        setLastActivityOn(userGit.getLastActivityOn());
        setLastSignInAt(userGit.getLastSignInAt());
        setLinkedin(userGit.getLinkedin());
        setLocation(userGit.getLocation());
        setName(userGit.getName());
        setOrganization(userGit.getOrganization());
        setPrivateProfile(userGit.getPrivateProfile());
        setProjectsLimit(userGit.getProjectsLimit());
        setProvider(userGit.getProvider());
        setPublicEmail(userGit.getPublicEmail());
        setSharedRunnersMinutesLimit(userGit.getSharedRunnersMinutesLimit());
        setSkipConfirmation(userGit.getSkipConfirmation());
        setSkype(userGit.getSkype());
        setState(userGit.getState());
        setThemeId(userGit.getThemeId());
        setTwitter(userGit.getTwitter());
        setTwoFactorEnabled(userGit.getTwoFactorEnabled());
        setUsername(userGit.getUsername());
        setWebUrl(userGit.getWebUrl());
        setWebsiteUrl(userGit.getWebsiteUrl());

    }

    
    /** 
     * Connect a User
     * @param userAPI
     * @param username
     * @param password
     * @return String Return User Token
     * @throws GitLabApiException
     */
    private String getAuthToken(UserAPI userAPI, String username, String password) throws GitLabApiException {
            return userAPI.Connect(username, password).getAccessToken();

    }

    
    /**
     * Add a project to the list 
     * @param project
     */
    public void addProject(Project project) {
        this.projects.add(project);
    }

    
    /** 
     * Remove a project to the list 
     * @param project
     */
    public void removeProject(Project project) {
        this.projects.remove(project);

    }

    
    /** 
     * Getter for a single project
     * @param index index of the project in the list
     * @return Project
     */
    public Project getProject(int index) {
        return this.projects.get(index);
    }

    
    /** 
     * Fetch project list from the api
     * @param userAPI
     * @param withStatistic enable statistic
     */
    public void getProject(UserAPI userAPI, boolean withStatistic) {

        try {
            this.projects = (ArrayList<Project>) userAPI.getUserProjet(token, getId(), withStatistic);
        } catch (GitLabApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    
    /** 
     * @return String
     */
    @Override
    public String toString() {
        String disp = "User :" + getUsername() + '\n';
        if (projects != null) {
            for (Project project : projects) {
                disp = disp + project.toString() + '\n';
            }
            
        }
        return disp;
    }

    
    /** 
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof UserGit))
            return false;
        UserGit u = (UserGit) obj;
        if (!(this.getEmail().equals(u.getEmail())))
            return false;
        if (this.getId() != u.getId())
            return false;
        return true;

    }
}
