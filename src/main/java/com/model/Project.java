package com.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Branch;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.Member;
import org.gitlab4j.api.models.TreeItem;
import org.gitlab4j.api.models.Visibility;

import com.view.ProjectView;


public class Project extends org.gitlab4j.api.models.Project {
	private ArrayList<UserGit> userGits;

	private String group;
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	
	public Project() {
		super();
	}

	public Project(int id, String name, String group, String visibility, ArrayList<UserGit> userlist) {
		super();
		super.setId(id);
		super.setName(name);
		super.setVisibility(Visibility.forValue(visibility));
		this.userGits = userlist;
		setName(name);
		setGroup(group);
	}

	public Project(int id, String name, String visibility) {
		super();
		super.setId(id);
		super.setName(name);
		super.setVisibility(Visibility.forValue(visibility));
		this.userGits = new ArrayList<UserGit>();
	}

	public Project(int id, String name) {
		super();
		super.setId(id);
		super.setName(name);
		super.setVisibility(Visibility.forValue("1"));
		this.userGits = new ArrayList<UserGit>();
	}
	public Project(Project p){}
	
	/** 
	 * Return a user from the list 
	 * @param index index of the user in the list
	 * @return UserGit 
	 */
	public UserGit getUser(int index) {

		return this.userGits.get(index);
	}

	
	/** 
	 * getter user list
	 * @return ArrayList<UserGit>
	 */
	public ArrayList<UserGit> getUser() {

		return this.userGits;
	}
	
	/** 
	 * Fetch users from gitlab who are members of the project
	 * @param id project's id
	 * @param gitLabApi RestAPI
	 * @return List<Member> list of members
	 * @throws GitLabApiException
	 */
	public List<Member> getUsers(int  id) throws GitLabApiException{
		List<Member> allMembers = ConnexionModel.getGitLabApi().getProjectApi().getAllMembers(id);
		for (Member M : allMembers) {
			System.out.println(M.getGroupSamlIdentity());
		}
		return ConnexionModel.getGitLabApi().getProjectApi().getAllMembers(id);

	}

	
	/** 
	 * Add user from the list 
	 * @param user
	 */
	public void addUser(UserGit user) {
		this.userGits.add(user);

	}

	
	/** 
	 * remove user from the list
	 * @param user
	 */
	public void removeUser(UserGit user) {
		this.userGits.remove(user);

	}


	
	/** 
	 * @return String
	 */
	@Override
	public String toString() {
		String ts = "Project : " + getName();
		ts += "\n Group : " + group;
		if (userGits != null) {
			for (UserGit x : userGits) {
				ts += "\n User : " + x.getUsername();
			}
		}
		return ts;
	}

	
	/** 
	 * @param obj
	 * @return boolean
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Project))
			return false;
		Project p = (Project) obj;
		if (!(this.getName().equals(p.getName())))
			return false;
		if (this.getId() != p.getId())
			return false;
		return true;
	}

	
	/** 
	 * Fetch project's branches 
	 * @param id project id
	 * @return List<String>
	 * @throws GitLabApiException
	 */
	public List<String> getBranches(int id) throws GitLabApiException {
		List<String> branches = new ArrayList<>();
		for (Branch B : ConnexionModel.getGitLabApi().getRepositoryApi().getBranches(id)) {
			branches.add(B.getName());
		}
		return branches;
	}


	/** 
	 * Fetch a folder from a branch
	 * @param idOrPath id or path of the project
	 * @param branche name of a branche
	 * @return List<String> List of item in the folder
	 * @throws GitLabApiException
	 */
	public List<String> getFolder(Object idOrPath, String branche) throws GitLabApiException {

		List<String> dossier = new ArrayList<>();
		// Pour chaque dossier/fichier de notre branche
		for (TreeItem T : ConnexionModel.getGitLabApi().getRepositoryApi().getTree(idOrPath, "/", branche)) {
			// On ajoute tous les dossiers à la liste dossier
			if (T.getType() == TreeItem.Type.TREE) {
				dossier.add(T.getPath());
			}
		}
		return dossier;
	}

	
	/** 
	 * Fetch a folder from a branch
	 * @param idOrPath id of the project 
	 * @param path path of the project 
	 * @param branche branch name
	 * @return List<String>
	 * @throws GitLabApiException
	 */
	// Surcharge de la méthode getFolder
	public List<String> getFolder(Object idOrPath, String path, String branche)
			throws GitLabApiException {
		List<String> dossier = new ArrayList<>();
		// Pour chaque dossier/fichier de notre branche
		for (TreeItem T : ConnexionModel.getGitLabApi().getRepositoryApi().getTree(idOrPath, path, branche, false)) {
			// On ajoute tous les dossiers à la liste dossier
			if (T.getType() == TreeItem.Type.TREE) {
				dossier.add(T.getPath());
			}
		}
		return dossier;
	}

	
	/** 
	 * Fetch a file from a branch
	 * @param idOrPath
	 * @param branche
	 * @return List<String>
	 * @throws GitLabApiException
	 */
	public List<String> getFile(Object idOrPath, String branche) throws GitLabApiException {

		List<String> fichier = new ArrayList<>();
		// Pour chaque dossier/fichier de notre branche on affiche
		for (TreeItem T : ConnexionModel.getGitLabApi().getRepositoryApi().getTree(idOrPath, "/", branche)) {
			// On ajoute tous les fichiers à la liste fichier
			if (T.getType() == TreeItem.Type.BLOB) {
				fichier.add(T.getPath());
			}
		}
		return fichier;
	}

	
	/** 
	 * Fetch a folder from a branch
	 * @param idOrPath
	 * @param path
	 * @param branche
	 * @return List<String>
	 * @throws GitLabApiException
	 */
	// Surcharge de la méthode getFile
	public List<String> getFile(Object idOrPath, String path, String branche)
			throws GitLabApiException {
		List<String> fichier = new ArrayList<>();
		// Pour chaque dossier/fichier de notre branche
		for (TreeItem T : ConnexionModel.getGitLabApi().getRepositoryApi().getTree(idOrPath, path, branche, false)) {
			// On ajoute tous les fichiers à la liste fichier
			if (T.getType() == TreeItem.Type.BLOB) {
				fichier.add(T.getPath());
			}
		}
		return fichier;
	}


	/** 
	 * delete a project by its id
	 * @param gitLabApi
	 * @throws GitLabApiException
	 */
	public void deleteProject() throws GitLabApiException {
		ConnexionModel.getGitLabApi().getProjectApi().deleteProject(getId());

	}

	
	/** 
	 * getter groupname
	 * @return String
	 */
	public String getGroup() {
		return group;
	}

	
	/** 
	 * setter groupname
	 * @param group new groupname
	 */
	public void setGroup(String group) {
		this.group = group;
	}
	

	
	/** 
	 * Fetch the latest number of commit
	 * @param idOrPath id or path of the project
	 * @param branche branch name
	 * @return int number of commit
	 * @throws GitLabApiException
	 */
	public int getNumberCommits(Object idOrPath, String branche) throws GitLabApiException{
		return ConnexionModel.getGitLabApi().getCommitsApi().getCommits(idOrPath, branche, null).size();
	}
	
	
	/** 
	 * Fetch last commit of the project from gitlab 
	 * @param idOrPath id or path of the project
	 * @param branche branch name
	 * @return Commit
	 * @throws GitLabApiException
	 */	
	public Commit getLastCommit(Object idOrPath, String branche) throws GitLabApiException {
		List<Commit> commits = ConnexionModel.getGitLabApi().getCommitsApi().getCommits(idOrPath, branche, null);
		//Premier de la liste est le dernier commit dans Git
		return commits.get(0);
	}
	
	/** 
	 * Fetch languages of the project from gitlab 
	 * @param idOrPath id or path of the project
	 * @return Map<String, Float>
	 * @throws GitLabApiException
	 */
	public Map<String, Float> getProjectLanguages(Object idOrPath, GitLabApi gitLabApi) throws GitLabApiException{
		return ConnexionModel.getGitLabApi().getProjectApi().getProjectLanguages(idOrPath);
	}
	
	/** 
	 * @param listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
	 	this.pcs.addPropertyChangeListener(listener);
	 }

}