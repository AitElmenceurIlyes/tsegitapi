package com.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.AccessLevel;
import org.gitlab4j.api.models.Group;
import org.gitlab4j.api.models.GroupParams;
import org.gitlab4j.api.models.ProjectFilter;

import com.controller.GitGudExceptions;
import com.controller.JsonParser;
import com.view.DashboardView;

public class DashboardModel {
	
	
	ArrayList<org.gitlab4j.api.models.Project> myProjects;
	ArrayList<Group> myGroups ;
	Module[] myModules ;
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	
	/** 
	 * @param listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public DashboardModel() {
		try {
			myProjects = new ArrayList<org.gitlab4j.api.models.Project>(ConnexionModel.getGitLabApi().getProjectApi().getProjects(new ProjectFilter().withMembership(true)));
			myGroups = new ArrayList<Group>(ConnexionModel.getGitLabApi().getGroupApi().getGroups());
			JsonParser.getInstance();
			myModules = (Module[]) JsonParser.readFile(Module[].class, "./ModuleModel.json");
			
		} catch (GitLabApiException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}
	

	
	/** 
	 * @return List<Project>
	 */
	public List<org.gitlab4j.api.models.Project> getprojects() {
		return myProjects;
	}

	public void updateProjects() {
		List<org.gitlab4j.api.models.Project> oldprojects = myProjects;
		try {
			myProjects = new ArrayList<org.gitlab4j.api.models.Project>(ConnexionModel.getGitLabApi().getProjectApi().getProjects(new ProjectFilter().withMembership(true)));
		} catch (GitLabApiException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		this.pcs.firePropertyChange("Myprojects", oldprojects, myProjects);

	}

	
	/** 
	 * @param csv_file
	 * @return ArrayList<Project>
	 * @throws GitGudExceptions 
	 */
	public Map<String, ArrayList<Project>> getProjectsFromCsv(File csv_file) throws GitGudExceptions {
		// Liste à retourner
		Map<String, ArrayList<Project>> groups = new HashMap<String, ArrayList<Project>>();
		ArrayList<Project> projects = new ArrayList<Project>();
		String oldGroupName = null;
		String oldProjectName = null;
		Boolean init = true;
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(csv_file));
			CSVParser csvParser = new CSVParser(br, CSVFormat.EXCEL.withDelimiter(';').withHeader());

			// Pour chaque entrée du fichier CSV
			for (CSVRecord csvRecord : csvParser) {
				// Accéder aux valeurs par les HEADERS du CSV
				String username = csvRecord.get("username");
				String groupName = csvRecord.get("group");
				String projectName = csvRecord.get("project");
				
				if(init) {
					oldGroupName = groupName;
					oldProjectName = projectName;
					groups.put(groupName, projects);
					groups.get(groupName).add(new Project(projects.size(), projectName, "1"));
					groups.get(groupName).get(0).addUser(new UserGit(1, username));
					groups.get(groupName).get(0).setGroup(groupName);
					init = false;
				}
				else {
				
					// Si il s'agit du même group : ajout des projet
					if(groupName.equals(oldGroupName)) {				
						// Si il s'agit du même projet : ajout des username
						if (projectName.equals(oldProjectName)) {
							groups.get(groupName).get(projects.size() - 1).addUser(new UserGit(1, username));
							groups.get(groupName).get(projects.size() - 1).setGroup(groupName);
						}
						// Sinon, création d'un nouveau projet et ajout des attributs
						else {
							groups.get(groupName).add(new Project(projects.size(), projectName, "1"));
							groups.get(groupName).get(projects.size() - 1).addUser(new UserGit(1, username));
							groups.get(groupName).get(projects.size() - 1).setGroup(groupName);
						}
						oldProjectName = projectName;
					}
					// Sinon, cloture du Group puis création du group et ajout du nouveau projet 
					else {
						projects = new ArrayList<Project>();
						groups.put(groupName, projects);
						groups.get(groupName).add(new Project(projects.size(), projectName, "1"));
						groups.get(groupName).get(0).addUser(new UserGit(1, username));
						groups.get(groupName).get(0).setGroup(groupName);
						oldProjectName = projectName;
					}
					oldGroupName = groupName;
				}
			}
			csvParser.close();
		} catch (Exception e) {
			throw new GitGudExceptions(e.getMessage());
		}
		return groups;
	}

	
	/** 
	 * Convert a CSV project list to Gitlab projects and groups
	 * @param csv_file
	 */
	public String addProjectsFromCsv(File csv_file) throws GitGudExceptions{
		String logs = "";
		Map<String, ArrayList<Project>> groupList = this.getProjectsFromCsv(csv_file);
		
		// Itération sur les groups
		for (Entry<String, ArrayList<Project>> group : groupList.entrySet()) {
			int groupId = 0;
			try {
				groupId = ConnexionModel.getGitLabApi().getGroupApi().addGroup(group.getKey(), group.getKey()).getId();
				// Itération sur les projet
				for (Project project : group.getValue()) {
					
					org.gitlab4j.api.models.Project projectSpecs = new org.gitlab4j.api.models.Project().withName(project.getName()).withPublic(false);
					org.gitlab4j.api.models.Project newProject = null;
					try {
						// Création du projet sur gitlab
						newProject = ConnexionModel.getGitLabApi().getProjectApi().createProject(groupId, projectSpecs);
						
						// Itération sur les user
						for (UserGit user : project.getUser()) {
							// Exception : Si l'utilisateur n'existe pas
							List<UserGit> memberList = ConnexionModel.getUserApi().getUserByUsername(user.getUsername());
							if (memberList.size() > 0) {
								int newMemberId = memberList.get(0).getId();
								// Exception : Si l'utilisateur appartient déjà au projet
								try {
									ConnexionModel.getGitLabApi().getProjectApi().addMember(newProject, newMemberId, AccessLevel.MAINTAINER);
									
								} catch (GitLabApiException e) {
									// L'utilisateur appartient déjà au projet
								}
							}
							else {
								logs += "ERROR: GitLab user < " + user.getUsername() + " > from project < " + project.getName() + " > does not exist: add aborted. \n";
							}
						}
					} catch (GitLabApiException e1) {
						logs += "ERROR: GitLab project < " + project.getName() + " > from group < " + group.getKey() + " > already exists: creation failed. \n";
					}
		
				}
			} catch (GitLabApiException e2) {
				logs += "ERROR: GitLab group < " + group.getKey() + " > already exists: creation failed. (No project created)\n";
			}
		}
		return logs;
	}

	
	/** 
	 * @param id
	 * @return Group
	 */
	public Group getGroup(int id) {
		Group foundGroup = null;
		for (Group group : myGroups) {
			if (group.getId() == id) {
				foundGroup = group;
				
			}
		}
		return foundGroup;
    }
	
	
	
	/** 
	 * @return List<Group>
	 */
	public List<Group> getGroups() {
        return myGroups;
    }

    public void updateGroups() {
		try {
			myGroups = new ArrayList<Group>(ConnexionModel.getGitLabApi().getGroupApi().getGroups());
			
		} catch (GitLabApiException e2) {
			// TODO Auto-generated catch block
		e2.printStackTrace();
		}
    }
	    
    
	/** 
	 * @param id
	 * @return List<Group>
	 */
	public List<Group> getGroups(Integer id){
        ArrayList<Group> myGroupsTemp = new ArrayList<Group> ();
        for (Group group : myGroups) {
//	            System.out.println("id of the group "  + group.getId());
//	            System.out.println("ids"  + group.getParentId() + "and" + id + "." );
//	            System.out.println( group.getParentId() == id );
            if (id == null) {
                if (group.getParentId() == id) {
                    myGroupsTemp.add(group);
//	                    System.out.println("parentid : " + group.getParentId());
                }

            }else {
                if (group.getParentId() != null)

                    if (group.getParentId().intValue() == id.intValue()) {
                        myGroupsTemp.add(group);
//	                        System.out.println("parentid : " + group.getParentId());
                    }
            }
        }
        //if myGroupsTemp == null {
        return myGroupsTemp;
    }
	
	
	/** 
	 * @return ModuleModel[]
	 */
	public Module[] getModules() {
		return myModules;
	}

	public void updateModules() {
		Module[] oldModules = myModules;
		myModules = (Module[]) JsonParser.readFile(Module[].class, "./ModuleModel.json");
		this.pcs.firePropertyChange("Myprojects", oldModules, myModules);

	}

}
