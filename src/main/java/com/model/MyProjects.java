package com.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.ProjectFilter;
import org.gitlab4j.api.models.AccessLevel;
import org.gitlab4j.api.models.GroupParams;

import com.controller.GitGudExceptions;
import com.controller.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MyProjects {

	List<Project> Myprojects;
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	
	/** 
	 * @param listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}
	public MyProjects() {
		try {

			refreshMyProjects(ConnexionModel.getGitLabApi().getProjectApi().getProjects(new ProjectFilter()
																						.withMembership(true)
																						.withOrderBy(Constants.ProjectOrderBy.forValue("LAST_ACTIVITY_AT "))));
					}
		catch (GitLabApiException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}

	
	/** 
	 * Transform a list of gitlab project into a list of java project
	 * @param list
	 */
	public void refreshMyProjects(List<org.gitlab4j.api.models.Project> list) {
		try {
			ObjectMapper objectWriter = JsonParser.getInstance().getObjectMapper();
			objectWriter.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonString = objectWriter.writerWithDefaultPrettyPrinter()
					.writeValueAsString(list);
			Myprojects = new ArrayList<Project>(Arrays.asList(objectWriter.readValue(jsonString, Project[].class)));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public List<Project> getProjects() {
		return Myprojects;
	}


	/** 
	 * Update the list of project
	 * @param gitLabApi
	 */
	public void update() {
//		List<Project> oldprojects = Myprojects;
		try {
			refreshMyProjects(ConnexionModel.getGitLabApi().getProjectApi().getProjects(new ProjectFilter()
					.withMembership(true)
					.withOrderBy(Constants.ProjectOrderBy.forValue("LAST_ACTIVITY_AT "))));

		} catch (GitLabApiException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//		if (!oldprojects.equals(Myprojects)) {
//			this.pcs.firePropertyChange("Myprojects", oldprojects, Myprojects);
//		}

	}

	
	/** 
	 * Fetch list of project from the connected user
	 * @param gitlaAPI
	 * @param archived filter between archived and unarchived project
	 * @throws GitLabApiException
	 */
	public void getProjects(Boolean archived) throws GitLabApiException{
		try {
            refreshMyProjects(ConnexionModel.getGitLabApi().getProjectApi()
			.getProjects(new ProjectFilter()
            .withArchived(archived)
            .withMembership(true)));
        } catch (GitLabApiException e) {
            throw e;
        }
	}
	
	public void deleteProjects(ArrayList<Integer> idList) throws GitLabApiException {
		try {
			for (Integer id : idList) {
				ConnexionModel.getGitLabApi().getProjectApi().deleteProject(id);
			}
        } catch (GitLabApiException e) {
            throw e;
        }
		
	}
	
	public void shareProjects(ArrayList<Integer> idList,int groupId, AccessLevel accessLevel) throws GitLabApiException {
		try {
			for (Integer id : idList) {
				ConnexionModel.getGitLabApi().getProjectApi().shareProject(id, groupId, accessLevel, null);
			}
        } catch (GitLabApiException e) {
            throw e;
        }
		
	}

	
	/** 
	 * @param csv_file
	 * @return ArrayList<Project>
	 * @throws GitGudExceptions 
	 */
	public Map<String, ArrayList<Project>> getProjectsFromCsv(File csv_file) throws GitGudExceptions {
		// Liste à retourner
		Map<String, ArrayList<Project>> groups = new HashMap<String, ArrayList<Project>>();
		ArrayList<Project> projects = new ArrayList<Project>();
		String oldGroupName = null;
		String oldProjectName = null;
		Boolean init = true;
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(csv_file));
			CSVParser csvParser = new CSVParser(br, CSVFormat.EXCEL.withDelimiter(';').withHeader());

			// Pour chaque entrée du fichier CSV
			for (CSVRecord csvRecord : csvParser) {
				// Accéder aux valeurs par les HEADERS du CSV
				String username = csvRecord.get("username");
				String groupName = csvRecord.get("group");
				String projectName = csvRecord.get("project");
				
				if(init) {
					oldGroupName = groupName;
					oldProjectName = projectName;
					groups.put(groupName, projects);
					groups.get(groupName).add(new Project(projects.size(), projectName, "1"));
					groups.get(groupName).get(0).addUser(new UserGit(1, username));
					groups.get(groupName).get(0).setGroup(groupName);
					init = false;
				}
				else {
				
					// Si il s'agit du même group : ajout des projet
					if(groupName.equals(oldGroupName)) {				
						// Si il s'agit du même projet : ajout des username
						if (projectName.equals(oldProjectName)) {
							groups.get(groupName).get(projects.size() - 1).addUser(new UserGit(1, username));
							groups.get(groupName).get(projects.size() - 1).setGroup(groupName);
						}
						// Sinon, création d'un nouveau projet et ajout des attributs
						else {
							groups.get(groupName).add(new Project(projects.size(), projectName, "1"));
							groups.get(groupName).get(projects.size() - 1).addUser(new UserGit(1, username));
							groups.get(groupName).get(projects.size() - 1).setGroup(groupName);
						}
						oldProjectName = projectName;
					}
					// Sinon, cloture du Group puis création du group et ajout du nouveau projet 
					else {
						projects = new ArrayList<Project>();
						groups.put(groupName, projects);
						groups.get(groupName).add(new Project(projects.size(), projectName, "1"));
						groups.get(groupName).get(0).addUser(new UserGit(1, username));
						groups.get(groupName).get(0).setGroup(groupName);
						oldProjectName = projectName;
					}
					oldGroupName = groupName;
				}
			}
			csvParser.close();
		} catch (Exception e) {
			throw new GitGudExceptions(e.getMessage());
		}
		return groups;
	}

	
	/** 
	 * Convert a CSV project list to Gitlab projects and groups
	 * @param csv_file
	 */
	public String addProjectsFromCsv(File csv_file) throws GitGudExceptions{
		String logs = "";
		Map<String, ArrayList<Project>> groupList = this.getProjectsFromCsv(csv_file);
		
		// Itération sur les groups
		for (Entry<String, ArrayList<Project>> group : groupList.entrySet()) {
			int groupId = 0;
			String[] subgroups = group.getKey().split("/");
			Integer parentGroupId = null;
			String path = "";
			for (String subgroup : subgroups) {
				try {
					path = path.concat(subgroup);
					parentGroupId = ConnexionModel.getGitLabApi().getGroupApi().createGroup(new GroupParams().withName(subgroup).withPath(subgroup).withParentId(parentGroupId)).getId();
					logs += "Success: Created GitLab subgroup < " + subgroup + " >\n";

				} catch (GitLabApiException g){
					try {
						parentGroupId = ConnexionModel.getGitLabApi().getGroupApi().getGroup(path).getId();
					} catch (GitLabApiException e) {
						logs += "ERROR: GitLab subgroup < " + subgroup + " > \n"+e.getMessage()+"\n";

					}

				}

			}

//				groupId = ConnexionModel.getGitLabApi().getGroupApi().addGroup("TestProjiet","groupTest-SubgroupTest-2020-2021").getId();
			// Itération sur les projet
			for (Project project : group.getValue()) {
				
				org.gitlab4j.api.models.Project projectSpecs = new org.gitlab4j.api.models.Project().withName(project.getName()).withPublic(false);
				org.gitlab4j.api.models.Project newProject = null;
				try {
					// Création du projet sur gitlab
					newProject = ConnexionModel.getGitLabApi().getProjectApi().createProject(parentGroupId, projectSpecs);
					logs += "Success: Project < " + project.getName() + " > added to group < " + group.getKey() + " >\n";

					// Itération sur les user
					for (UserGit user : project.getUser()) {
						// Exception : Si l'utilisateur n'existe pas
						List<UserGit> memberList = ConnexionModel.getUserApi().getUserByUsername(user.getUsername());
						if (memberList.size() > 0) {
							int newMemberId = memberList.get(0).getId();
							// Exception : Si l'utilisateur appartient déjà au projet
							try {
								ConnexionModel.getGitLabApi().getProjectApi().addMember(newProject, newMemberId, AccessLevel.MAINTAINER);
								logs +="Success: GitLab user < " + user.getUsername() + " > added to project < " + project.getName() + " > \n";
							} catch (GitLabApiException e) {
								// L'utilisateur appartient déjà au projet
							}
						}
						else {
							logs += "ERROR: GitLab user < " + user.getUsername() + " > from project < " + project.getName() + " > does not exist: add aborted. \n";
						}
					}
				} catch (GitLabApiException e1) {
					logs += "ERROR: GitLab project < " + project.getName() + " > from group < " + group.getKey() + " > already exists: creation failed. \n";
				}

			}
		}
		return logs;
	}



}
