package com.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;


import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;

import org.gitlab4j.api.models.GroupFilter;
import org.gitlab4j.api.models.AccessLevel;
import org.gitlab4j.api.models.Group;



public class MyGroups {

	List<Group> myGroups ;
	
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);     

	/** 
	 * @param listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {      
		this.pcs.addPropertyChangeListener(listener);
	}
	
	 public MyGroups() {

			
			try {
				myGroups = ConnexionModel.getGitLabApi().getGroupApi().getGroups();
				myGroups = getGroups(null);
				
			} catch (GitLabApiException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
	    }

    
	    public List<Group> getGroups() {
	        return myGroups;
	    }

	    
	    public List<Group> getGroups(Integer id){
	        ArrayList<Group> myGroupsTemp = new ArrayList<Group> ();
	        for (Group group : myGroups) {
//	            System.out.println("id of the group "  + group.getId());
//	            System.out.println("ids"  + group.getParentId() + "and" + id + "." );
//	            System.out.println( group.getParentId() == id );
	            if (id == null) {
	                if (group.getParentId() == id) {
	                    myGroupsTemp.add(group);
//	                    System.out.println("parentid : " + group.getParentId());
	                }

	            }else {
	                if (group.getParentId() != null)

	                    if (group.getParentId().intValue() == id.intValue()) {
	                        myGroupsTemp.add(group);
//	                        System.out.println("parentid : " + group.getParentId());
	                    }
	            }
	        }
	        //if myGroupsTemp == null {
	        return myGroupsTemp;
	    }
	    
		public Group getGroup(int id) {
			Group foundGroup = null;
			for (Group group : myGroups) {
				if (group.getId() == id) {
					foundGroup = group;
					
				}
			}
			return foundGroup;
	    }

		public Group getGroup(String FullName) {
			Group foundGroup = null;
			for (Group group : myGroups) {
				if (group.getFullName() == FullName) {
					foundGroup = group;
				}
			}
			return foundGroup;
	    }
		
		public String[] getGroupsNames() {
			this.update();
			String[] names = new String[myGroups.size()];
			int i=0;
			for (Group group : myGroups) {
				names[i] = group.getFullName();
				i++;
			}
			return  names;
		}
		

	    public void update() {
	    		List<Group> oldGroups = myGroups;
			try {
				myGroups = ConnexionModel.getGitLabApi().getGroupApi().getGroups();
				
			} catch (GitLabApiException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
    		if (!oldGroups.equals(myGroups)) {
    			this.pcs.firePropertyChange("Myprojects", oldGroups, myGroups);
    		}
	    }
	    

	    
	    
    

    
}