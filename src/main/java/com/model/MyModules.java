package com.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;

import com.controller.JsonParser;



public class MyModules {
	
	private Module[] myModules;
	
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);     

	/** 
	 * @param listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {      
		this.pcs.addPropertyChangeListener(listener);
	}
	
	
	public MyModules(){
		myModules = (Module[]) JsonParser.readFile(Module[].class, "./ModuleModel.json");
	}


	public Module[] getModules() {
		return myModules;
	}
	
	public Module getModule(String moduleName) {
		Module foundModule = null;
		for (Module module : myModules) {
			if (module.getName() == moduleName) {
				foundModule = module;
				
			}
		}
		return foundModule;
    }
	
	
	public String[] getModulesNames() {
		this.update();
		String[] names = new String[myModules.length];
		int i=0;
		for (Module module : myModules) {
			names[i] = module.getName();
			i++;
		}
		return  names;
	}
	
	public int getModuleIndex(String moduleName) {
		int foundIndex = -1;
		for (int i = 0; i < myModules.length; i++) {
			if (myModules[i].getName() == moduleName) {
				foundIndex = i;	
			}
		}
		return foundIndex;
	}
	
	public void saveToFile(Module[] Modules) {
        JsonParser.writeFile(Modules, "./ModuleModel.json");
	}
	
	public void addGroupsToModule(ArrayList<Integer> idList,Module module, MyGroups myGroups) {

		for (Integer id : idList) {
			com.model.Group group = new com.model.Group(myGroups.getGroup(id));
			module.addGroup(group);
			updateMyModules(module);
			saveToFile(myModules);
		}
				
	}
	
	public void removeGroupsToModule(ArrayList<Integer> idList,Module module, MyGroups myGroups){

		for (Integer id : idList) {
			com.model.Group group = new com.model.Group(myGroups.getGroup(id));
			module.addGroup(group);
			updateMyModules(module);
			saveToFile(myModules);
		}
				
	}
	
	public void deleteModules(ArrayList<Integer> indexList)  {
		Module[] newMyModules = myModules;
		Module[] newMyModules2 = null;
		System.out.println(indexList.toString());
		for (Integer index : indexList) {
			newMyModules2 = new Module[newMyModules.length - 1];
			for (int i = 0, j=0; i < newMyModules.length; i++) {
				if (i != index) {
//					System.out.println(i);
//					System.out.println(index);
					newMyModules2[j++] = newMyModules[i];
			    }
			}
 
			newMyModules = newMyModules2;

		}
		myModules = newMyModules2;
//		System.out.println(newMyModules2[0].toString()); 
//		System.out.println(newMyModules2[1].toString()); 

		saveToFile(myModules);
		update();
		
	}
	
	public void updateMyModules(Module module) {
		int i = getModuleIndex(module.getName());
		myModules[i] = module;
	}

	public void update() {
		Module[] oldModules = myModules;
		myModules = (Module[]) JsonParser.readFile(Module[].class, "./ModuleModel.json");
		this.pcs.firePropertyChange("Myprojects", oldModules, myModules);

	}
}
