package com.model;

import org.gitlab4j.api.GitLabApi;

import com.api.controler.UserAPI;

public class ConnexionModel {
	private static final String secretEncryptKey = "H<FVf836u^!;uJW{h97c";
	private static final String dbConnectPath = "./userConnect_db.json";
	private static final String configPath = "./config.json";
	private static String gitlab_default_URL = "";
	private static String username = "";
	private static UserGit userGitApi = null;
	private static UserAPI userApi = null;
	private static GitLabApi gitLabApi = null;
	private static Boolean isALocalUser = false;

	/** 
	 * Return default Gitlab instance URL
	 * @return String
	 */
	public static String getGitlab_default_URL() {
		return gitlab_default_URL;
	}

	/** Set default Gitlab instance URL
	 * @param gitlab_default_URL
	 */
	public static void setGitlab_default_URL(String gitlab_default_URL) {
		ConnexionModel.gitlab_default_URL = gitlab_default_URL;
	}

	/** 
	 * Get secret key to encrypt sensible connection information
	 * @return String
	 */
	public static String getSecretEncryptKey() {
		return secretEncryptKey;
	}

	/** 
	 * Return local user JSON database file path
	 * @return String
	 */
	public static String getDbconnectPath() {
		return dbConnectPath;
	}

	/** 
	 * Return the JSON config file path
	 * @return String
	 */
	public static String getConfigPath() {
		return configPath;
	}

	/** 
	 * Return the connected Gitlab4J instance
	 * @return UserGit
	 */
	public static UserGit getUserGitApi() {
		return userGitApi;
	}

	/** 
	 * Set the connected Gitlab4J instance
	 * @param userGitApi
	 */
	public static void setUserGitApi(UserGit userGitApi) {
		ConnexionModel.userGitApi = userGitApi;
	}

	/** 
	 * Get the connected Gitlab USER_API instance
	 * @return GitLabApi
	 */
	public static GitLabApi getGitLabApi() {
		return gitLabApi;
	}

	/** 
	 * Set the connected Gitlab USER_API instance
	 * @param gitLabApi
	 */
	public static void setGitLabApi(GitLabApi gitLabApi) {
		ConnexionModel.gitLabApi = gitLabApi;
	}

	/** 
	 * Return true if the connected user is a Local user type
	 * @return Boolean
	 */
	public static Boolean getIsALocalUser() {
		return isALocalUser;
	}

	/** 
	 * Set the type of user: True Local type user or False Gitlab type user
	 * @param localUser
	 */
	public static void setIsALocalUser(Boolean localUser) {
		ConnexionModel.isALocalUser = localUser;
	}

	/** 
	 * Return the connected username
	 * @return String
	 */
	public static String getUsername() {
		return username;
	}

	/** 
	 * Set the connected username
	 * @param username
	 */
	public static void setUsername(String username) {
		ConnexionModel.username = username;
	}

	public static UserAPI getUserApi() {
		return userApi;
	}

	public static void setUserApi(UserAPI userApi) {
		ConnexionModel.userApi = userApi;
	}

}
