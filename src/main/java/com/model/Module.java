package com.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Module {
    @JsonProperty("Id")
    private Integer id;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Groups")
    private ArrayList<Group> groups;
    @JsonProperty("archived")
    private Boolean isArchived;

    public Module() {

    }

    
    /** 
     * @return Boolean
     */
    public Boolean getArchived() {
        return isArchived;
    }

    
    /** 
     * @param isArchived
     */
    public void setArchived(Boolean isArchived) {
        this.isArchived = isArchived;
    }

    public Module(Module copy) {
        this.groups = copy.groups;
        this.id = copy.id;
        this.name = copy.name;
        this.isArchived = copy.isArchived;
    }

    public Module(int id, String name) {
        this.id = id;
        this.name = name;
        this.groups = new ArrayList<Group>();
        this.isArchived =false;
    }
    public Module(int id, String name,boolean arc) {
        this.id = id;
        this.name = name;
        this.groups = new ArrayList<Group>();
        this.isArchived =arc;
    }

    public Module(int id, String name, ArrayList<Group> groups) {
        this.id = id;
        this.name = name;
        this.groups = groups;
    }
    public Module(int id, String name, ArrayList<Group> groups,boolean arc) {
        this.id = id;
        this.name = name;
        this.groups = groups;
        this.isArchived = arc;
    }

    
    /** 
     * @return int
     */
    public int getId() {
        return id;
    }

    
    /** 
     * @return String
     */
    public String getName() {
        return name;
    }

    
    /** 
     * @param index
     * @return Group
     */
    public Group getGroup(int index) {
        return groups.get(index);
    }

    
    /** 
     * @return ArrayList<Group>
     */
    public ArrayList<Group> getGroups() {
        return groups;
    }

    
    /** 
     * @param groups_
     */
    public void setGroups(ArrayList<Group> groups_) {
        this.groups = groups_;
    }

    
    /** 
     * @param group
     */
    public void addGroup(Group group) {
        this.groups.add(group);
    }

    
    /** 
     * @param group
     */
    public void removeGroup(Group group) {
        this.groups.remove(group);
    }

    
    /** 
     * @param mList
     * @return ArrayList<ModuleModel>
     */
    public static ArrayList<Module> listToArchivate(ArrayList<Module> mList) {
        ArrayList<Module> archlist = new ArrayList<Module>();
        for (Module m : mList) {
            if (m.getArchived())
                archlist.add(m);
        }
        return archlist;
    }

    
    /** 
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Module))
            return false;
        Module m = (Module) obj;
        if (this.id != m.getId())
            return false;
            if (!(name.equals(m.getName())))
            return false;
        if (!(isArchived.equals(m.getArchived())))
            return false;
        return true;
    }

    
    /** 
     * @return String
     */
    @Override
    public String toString() {
        String ts = "Module : " + name;
        ts += "\nisArchived : " + " "+this.isArchived;
        ts += "\nGroup : ";
        for (Group x : groups) {
            ts += x.getName() + " ";
        }
        return ts;
    }

}
