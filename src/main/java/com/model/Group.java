package com.model;

import java.beans.ConstructorProperties;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Group extends org.gitlab4j.api.models.Group{
    @JsonProperty("Name")	
    private String name;
    @JsonProperty("Id")	
    private Integer id;
    private ArrayList<Project> projects;
    private ArrayList<UserGit> userGits;
    public Group() {}
    public Group(Integer id, String groupName, ArrayList<Project> projects, ArrayList<UserGit> userGits) {
        this.id = id;
        this.name = groupName;
        this.projects = projects;
        this.userGits = userGits;
    }
    @ConstructorProperties({"Id", "Name"})
    public Group(Integer id, String groupName) {
        this.id = id;
        this.name = groupName;
        this.projects = new ArrayList<Project>();
        this.userGits = new ArrayList<UserGit>();
    }
    
    public Group(org.gitlab4j.api.models.Group group) {
        this.id = group.getId();
        this.name = group.getFullName();
        this.projects = new ArrayList<Project>();
        this.userGits = new ArrayList<UserGit>();
    }

    
    /** 
     * @return String
     */
    public String getName() {
        return name;
    }
    
    /** 
     * @param name_
     */
    public void setName(String name_) {
        this.name= name_;
    }
    
    /** 
     * @return Integer
     */
    public Integer getId() {
        return id;
    }
    
    /** 
     * @param id_
     */
    public void setId(Integer id_) {
        this.id= id_;
    }
    
    /** 
     * @param index
     * @return Project
     */
    public Project getProject(int index) {
        return projects.get(index);
    }

    
    /** 
     * @param project
     */
    public void addProject(Project project) {
        this.projects.add(project);
    }

    
    /** 
     * @param project
     */
    public void removeProject(Project project) {
        this.projects.remove(project);
    }

    
    /** 
     * @param index
     * @return UserGit
     */
    public UserGit getUser(int index) {
        return userGits.get(index);
    }

    
    /** 
     * @param userGit
     */
    public void addUserGit(UserGit userGit) {
        this.userGits.add(userGit);
    }

    
    /** 
     * @param userGit
     */
    public void removeUserGit(UserGit userGit) {
        this.userGits.remove(userGit);
    }

    
    /** 
     * @return String
     */
    @Override
    public String toString() {
        String ts = "Group : " + name;
        ts += "\nUser : ";
        for (UserGit x : userGits) {
            ts += x.getEmail()+" ";
        }
        ts+="\n"+ "Project : " ;
        for(Project p:  projects){
            ts+= p.getName()+" ";
        }
        return ts;
    }

    
    /** 
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Group))
            return false;
        Group g = (Group) obj;
        if (!(g.getName().equals(name)))
            return false;
        if (!(g.getId().equals(id)))
            return false;

        return true;
    }
}
