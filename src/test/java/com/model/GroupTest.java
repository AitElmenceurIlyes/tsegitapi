package com.model;



import com.model.Group;
import com.model.MyProjects;
import com.model.UserGit;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GroupTest {
    private Group g;
    @Before
    public void initialize() {
        g = new Group(1,"name");
     }
    @Test
    public void ConstructorTest()
    {
        Assert.assertEquals(g,new Group(1,"name"));
        Assert.assertNotEquals(g,new Group(2,"name"));
    }
    @Test
    public void AddTest()
    {
        Project p1 = new Project(1,"email");
        Project p2 = new Project(2,"email2");
        UserGit u1 = new UserGit(1,"email");
        UserGit u2 = new UserGit(2,"email2");
        g.addUserGit(u1);
        g.addUserGit(u2);
        g.addProject(p1);
        g.addProject(p2);
        Assert.assertEquals(g.getProject(0),p1);
        Assert.assertEquals(g.getUser(0),u1);
        g.removeProject(p1);
        g.removeUserGit(u1);
        Assert.assertNotEquals(g.getUser(0),p1);
        Assert.assertNotEquals(g.getProject(0),u1);
    }
    
}
