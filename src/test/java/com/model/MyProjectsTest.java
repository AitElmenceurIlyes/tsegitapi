package com.model;
//
//import static org.junit.Assert.*;

import java.io.File;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.model.MyProjects;
import com.model.Project;
import com.model.UserGit;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

//public class MyProjectsTest {
//
//	private static final String SAMPLE_CSV_FILE_PATH = "./gitlab-batch-creation-test.csv";
//	private String username= "le-delezir.aristide";
//    private String passwrd="Mesari97";
//    @Before
//    public void init() {
//        try {
//            ConnexionModel.setGitLabApi(GitLabApi.oauth2Login("https://code.telecomste.fr", username, passwrd));
//        } catch (GitLabApiException e) {
//            e.printStackTrace();
//        }
//    }
//	@Test
//	public void getProjectsFromCsvTest() {
//		MyProjects p = new MyProjects();
//		System.out.println(p);
//		ArrayList<Project> projectsList = p.getProjectsFromCsv(new
//		File(SAMPLE_CSV_FILE_PATH));
//		// Projet 1
//		Assert.assertEquals(projectsList.get(0).getName(), "awesomeproject1");
//		Assert.assertEquals(projectsList.get(0).getGroup(), "prinfo-info");
//		Assert.assertEquals(projectsList.get(0).getUser().get(0).getUsername(), "gonon.charlene");
//		Assert.assertEquals(projectsList.get(0).getUser().get(1).getUsername(), "arnaud.christophe");
//
//		// Projet 2
//		Assert.assertEquals(projectsList.get(1).getName(), "awesomeproject2");
//		Assert.assertEquals(projectsList.get(1).getGroup(), "prinfo-info");
//		Assert.assertEquals(projectsList.get(1).getUser().get(0).getUsername(), "chevalier.jules");
//		Assert.assertEquals(projectsList.get(1).getUser().get(2).getUsername(), "drevet.pierre-jean");
//
//		// Projet 3
//		Assert.assertEquals(projectsList.get(2).getName(), "superproject1");
//		Assert.assertEquals(projectsList.get(2).getGroup(), "prinfo-image");
//		Assert.assertEquals(projectsList.get(2).getUser().get(0).getUsername(), "laclau.charlotte");
//		Assert.assertEquals(projectsList.get(2).getUser().get(2).getUsername(), "itthirad.frederic");
//	}
//
//	@Test
//	public void addProjectsFromCsvTest() {
//		MyProjects p = new MyProjects();
//		try {
//			p.addProjectsFromCsv(new File("./liste.csv"));
//		} catch (GitLabApiException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		// Supprimer les projets créés
//	}
//
//}
