package com.model;


import com.model.Group;
import com.model.Module;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ModuleTest {
    private Module p;
    @Before
    public void initialize() {
        p = new Module(1, "name");
     }
    @Test
    public void ConstructorTest() {
        Assert.assertEquals(p, new Module(1, "name"));
        Assert.assertNotEquals(p, new Module(1, "email2"));
        Assert.assertNotEquals(p, new Module(2, "email2"));
    }

    @Test
    public void AddTest() {
        Module p = new Module(1, "name");
        Group g1 = new Group(1, "email");
        Group g2 = new Group(2, "email2");
        p.addGroup(g1);
        p.addGroup(g2);
        Assert.assertEquals(p.getGroup(0), g1);
        p.removeGroup(g1);
        Assert.assertNotEquals(p.getGroup(0), g1);
    }
}
