package com.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProjectGitTest {
    private Project p ;
    @Before
    public void initialize() {
        p = new Project(1,"name");
     }
    
    @Test
    public void ConstructorTest()
    {
        Assert.assertEquals(p,new Project(1,"name"));
        Assert.assertNotEquals(p,new Project(1,"email2"));
        Assert.assertNotEquals(p,new Project(2,"email2"));
    }
  
   
    
    /** 
     * @throws GitLabApiException
     * @throws IOException
     */
    @Test
    public void displayBranchesTest() throws GitLabApiException, IOException {
    	BufferedReader lecteur = null;
		String token = null;
		lecteur = new BufferedReader(new FileReader("token.txt"));
		token = lecteur.readLine();
		lecteur.close();
    	GitLabApi gitLabApi = new GitLabApi("https://code.telecomste.fr/", token);

    	//name
    	String name = gitLabApi.getProjectApi().getProject(1091).getNameWithNamespace().toString();
    	Project p = new Project(1091,name,"0");
    	// p.displayBranches(gitLabApi);
    }
    

    
    /** 
     * @throws IOException
     * @throws GitLabApiException
     */
    @Test
    public void getBranchesTest() throws IOException, GitLabApiException {
    	BufferedReader lecteur = null;
		String token = null;
		lecteur = new BufferedReader(new FileReader("token.txt"));
		token = lecteur.readLine();
		lecteur.close();
    	GitLabApi gitLabApi = new GitLabApi("https://code.telecomste.fr/", token);
    	
    	Project p1 = new Project(1091,gitLabApi.getProjectApi().getProject(1091).getNameWithNamespace().toString(),"0");
    	
    	List<String> brancheTest = new ArrayList<>();
    	brancheTest.add("branche1");
    	brancheTest.add("branche2");
    	brancheTest.add("main");
    	Assert.assertEquals(p1.getBranches(1091),brancheTest);
    	
    	brancheTest.remove(0);
    	Assert.assertNotEquals(p1.getBranches(1091), brancheTest);
    }
    

   
   /** 
    * @throws IOException
    * @throws GitLabApiException
    */
   //Fonctionne 
    public void deleteProjectTest() throws IOException, GitLabApiException {
       	BufferedReader lecteur = null;
    	String token = null;
    	lecteur = new BufferedReader(new FileReader("token.txt"));
    	token = lecteur.readLine();
    	lecteur.close();
        GitLabApi gitLabApi = new GitLabApi("https://code.telecomste.fr/", token);
        Project p = new Project(1430, gitLabApi.getProjectApi().getProject(1430).getNameWithNamespace().toString(),"0");
        p.deleteProject();
        Assert.assertNotEquals(p,new Project(1430,"testProject2","1"));
    }
}
    

