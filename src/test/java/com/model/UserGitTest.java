package com.model;

import java.net.MalformedURLException;
import java.net.URL;

import com.api.controler.UserAPI;

import org.gitlab4j.api.GitLabApiException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import io.github.cdimascio.dotenv.Dotenv;

public class UserGitTest {
    private UserGit u;
    private UserGit user;
    private String passwString;
    private String userString;
    private UserAPI userAPI;

    @Before
    public void initialize() {
        u = new UserGit(1, "username");
        try {
            Dotenv dotenv = Dotenv.configure().directory(".").load();
            passwString = dotenv.get("PASSWRD");
            userString = dotenv.get("USERNM");

            userAPI = new UserAPI(new URL("https://code.telecomste.fr"));
            user = new UserGit(userAPI, userString, passwString);
            
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (GitLabApiException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
    }

    @Test
    public void ConstructorTest() {
        Assert.assertEquals(u, new UserGit(1, "username"));
        Assert.assertNotEquals(u, new UserGit(2, "username2"));
        Assert.assertNotEquals(u, new UserGit(1, "username2"));
        Assert.assertEquals("ait-el-menceur.ilyes", user.getUsername());
    }

    @Test
    public void AddTest() {
        UserGit u = new UserGit(1, "email");
        Project p = new Project(1, "name");
        Project p2 = new Project(2, "name2");
        u.addProject(p);
        u.addProject(p2);
        Assert.assertEquals(u.getProject(0), p);
        u.removeProject(p);
        Assert.assertNotEquals(u.getProject(0), p);
    }

    @Test
    public void GetprojectTest() {
        user.getProject(userAPI ,false);
        System.out.println(user);
    }
}
