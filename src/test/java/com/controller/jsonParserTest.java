package com.controller;

import java.util.ArrayList;

import com.model.Group;
import com.model.Module;

import com.controller.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.controller.JsonParser;

public class jsonParserTest {
    private Module module1;
    private Module module2;
    private Module module3;
    private Group group1;
    private Group group2;
    @Before
    public void initialize(){
        JsonParser.getInstance(); 
        module1 = new Module(1, "module 1"); 
        module2 = new Module(2, "module 2");
        module3 = new Module(3, "module 3");
        group1 = new Group(2, "groupe 1");
        group2 = new Group(2, "groupe 2");

    }

    @Test
    public void writeFileTest(){
        
        module2.addGroup(group1);
        module2.addGroup(group2);
        module1.addGroup(group1);
        module1.addGroup(group2);
        module3.addGroup(group1);
        module3.addGroup(group2);
        module1.setArchived(true);
        module2.setArchived(true);
        module3.setArchived(false);

        ArrayList<Module> m =new ArrayList<Module>();
        m.add(module1);
        m.add(module2);
        m.add(module3);
        JsonParser.writeFile(m, "./ModuleModel.json");
        Assert.assertTrue(true);
    }
    @Test
    public void readFileTest(){
        Module module1 = new Module(1, "module 1");
        Module module2 = new Module(1, "module 2");
        Group group1 = new Group(1, "groupe 1");
        Group group2 = new Group(2, "groupe 2");
        module1.setArchived(true);
        module2.setArchived(false);
        module1.addGroup(group1);
        module1.addGroup(group2);
        Module[] p= (Module[]) JsonParser.readFile(Module[].class, "./ModuleModel.json");
        for(Module i : p) System.out.println(i);
        Assert.assertEquals(p[0],module1);
        Assert.assertNotEquals(p[1],module2);
    }
    
}
