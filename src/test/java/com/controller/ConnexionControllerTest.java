package com.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import io.github.cdimascio.dotenv.Dotenv;

public class ConnexionControllerTest {
    private Dotenv dotenv;
    private String username;
    private String passwrd;

    @Before
    public void init() {

        dotenv = Dotenv.configure().directory(".").load();
        username = dotenv.get("USERNM");
        passwrd = dotenv.get("PASSWRD");
    }

    @Test
    public void testLocalConnexion() {
        try {
            Assert.assertTrue(ConnexionController.localConnection(username, passwrd));
        } catch (GitGudExceptions e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
