package com.controller;

import com.model.ConnexionModel;
import com.model.MyProjects;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import io.github.cdimascio.dotenv.Dotenv;

public class DashboardControllerTest {
    private Dotenv dotenv;
    private String username;
    private String passwrd;
    private DashboardController dashboardController;
    @Before
    public void init() {
        
        dotenv = Dotenv.configure().directory(".").load();
        username=dotenv.get("USERNM");
        passwrd =dotenv.get("PASSWRD");
        try {
            ConnexionModel.setGitLabApi(GitLabApi.oauth2Login("https://code.telecomste.fr", username, passwrd));
        } catch (GitLabApiException e) {
            e.printStackTrace();
        }
        dashboardController = new DashboardController(new MyProjects());
    }
    
    /** 
     * @throws GitGudExceptions
     */
    @Test(expected = GitGudExceptions.class)
    public void testArchiveProject() throws GitGudExceptions {
        try {
            dashboardController.archiveProject(830);
            Assert.assertEquals(ConnexionModel.getGitLabApi().getProjectApi().getProject(830).getArchived(), true);
            dashboardController.archiveProject(11111111);

        } catch (GitLabApiException e) {
            throw new GitGudExceptions(e.getMessage());
        }
    }

    
    /** 
     * @throws GitGudExceptions
     */
    @Test
    public void testGetProjects() throws GitGudExceptions {
        try {
            // archived Project
            dashboardController.getProjects(true);
            Assert.assertEquals(Integer.valueOf(830), dashboardController.getProjects().getProjects().get(0).getId());
            // active Project
            dashboardController.getProjects(false);
            Assert.assertEquals(Integer.valueOf(1164), dashboardController.getProjects().getProjects().get(0).getId());
        } catch (GitGudExceptions e) {
            throw new GitGudExceptions(e.getMessage());
        }

    }

    
    /** 
     * @throws GitGudExceptions
     */
    @Test(expected = GitGudExceptions.class)
    public void testUnarchiveProject() throws GitGudExceptions {
        try {
            dashboardController.unarchiveProject(830);
            Assert.assertEquals(ConnexionModel.getGitLabApi().getProjectApi().getProject(830).getArchived(), false);
            dashboardController.unarchiveProject(11111111);

        } catch (GitGudExceptions e) {
            throw e;
        } catch (GitLabApiException e) {
            throw new GitGudExceptions(e.getMessage());
        }
    }
}
