package com.api;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import com.api.controler.UserAPI;
import com.model.Project;
import com.model.UserGit;

import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Key;
import org.gitlab4j.api.models.Membership;
import org.gitlab4j.api.models.OauthTokenResponse;
import org.junit.Before;
import org.junit.Test;

import io.github.cdimascio.dotenv.Dotenv;

import org.junit.Assert;

public class ServiceTest {
    private UserAPI userapi;
    private Dotenv dotenv;
    private String username;
    private String passwrd;
    @Before
    public void init() {
        try {
            userapi = new UserAPI(new URL("https://code.telecomste.fr"));
        
        dotenv = Dotenv.configure().directory(".").load();
        username=dotenv.get("USERNM");
        passwrd =dotenv.get("PASSWRD");
    } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Connection Test :-)
     */
    @Test
    public void ConnectTest() {
        OauthTokenResponse apiResponse;
        try {
            apiResponse = userapi.Connect(username,passwrd);
            System.out.println(apiResponse.getAccessToken());

            // should be null
        } catch (GitLabApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Test
    public void getUserByUsernameTest() {
        List<UserGit> apiResponse;
        try {
            apiResponse = userapi.getUserByUsername("ait-el-menceur.ilyes");
            System.out.println(apiResponse.get(0));
            Assert.assertEquals("AIT EL MENCEUR", apiResponse.get(0).getName());
            Assert.assertEquals("ait-el-menceur.ilyes", apiResponse.get(0).getUsername());
            Assert.assertTrue(276 == apiResponse.get(0).getId());

            // should be null
            apiResponse = userapi.getUserByUsername("");
        } catch (GitLabApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Test
    public void getUserByID() {
        UserGit apiResponse;
        try {
            apiResponse = userapi.getUserByID("1556e078bf2bd0b9f71f784c44bec7626730e5c86d5055a9aec3892bc9070fe5", 276);
            System.out.println(apiResponse);
            Assert.assertEquals("AIT EL MENCEUR", apiResponse.getName());
            Assert.assertEquals("ait-el-menceur.ilyes", apiResponse.getUsername());
            Assert.assertTrue(276 == apiResponse.getId());
            apiResponse = userapi.getUserByID("", 276);
        } catch (GitLabApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Test
    public void getUserProjetsTest() {
        List<Project> projects;
        try {
            projects = userapi.getUserProjet("1556e078bf2bd0b9f71f784c44bec7626730e5c86d5055a9aec3892bc9070fe5", 276, true);
                for (Project x:  projects) System.out.println(x);
        } catch (GitLabApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Test
    public void getUserMembershipTest() {
        try {
            List<Membership> apiResponse = userapi.getUserMembership("1556e078bf2bd0b9f71f784c44bec7626730e5c86d5055a9aec3892bc9070fe5", 276);
            System.out.println(apiResponse);
        } catch (GitLabApiException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void getUserKeyTest() {
        List<Key> keys;
        try {
            keys = userapi.getKey("1556e078bf2bd0b9f71f784c44bec7626730e5c86d5055a9aec3892bc9070fe5", 276);
            Assert.assertEquals("ilyes@PCAit", keys.get(0).getTitle());

        } catch (GitLabApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
